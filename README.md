# Track

This project was created to allow users save own location by using mobile phone.

## Technology

Backend: Spring, Hibernate, REST<br />
Frontend: Angular 2<br />
Mobile app: Android

## Overview

You can see results on http://track-frontend.herokuapp.com/<br />
Test user username: adam<br />
Test user password: adam<br />

## Further help

apoklade@gmail.com