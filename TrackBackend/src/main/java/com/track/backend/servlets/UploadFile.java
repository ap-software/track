package com.track.backend.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;



@WebServlet("/upload.do")
public class UploadFile extends HttpServlet {
	

	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(UploadFile.class);
	private static final String LINK_NAME = "downloadLink";
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer link = new StringBuffer("");
		HttpSession session = req.getSession();
		Process p = null;
		String command = "curl --upload-file /app/target/sqlite/track_db.sqlite https://transfer.sh/track_db.sqlite";
		System.out.println("command: "+command);
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
		} catch (InterruptedException | IOException e) {
			session.setAttribute(LINK_NAME, "http://www.upload.failed.com");
			resp.sendRedirect("jsp/uploaded.jsp");
			return;
		}
		String line = "";
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		if ((line = reader.readLine())!= null) {
			link.append(line);
		}
		session.setAttribute(LINK_NAME, link);
		resp.sendRedirect("jsp/uploaded.jsp");		
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
}
