package com.track.backend.utilities;

public class ExceptionUtils {
	
	
	/**
	 * Function prepare full stack trace of Throwable e
	 * and its causes exceptions as String.
	 * 
	 * @param e - Throwable
	 * @return string with full stack trace
	 */
	public static String fullStackTraceWithCauses(Throwable e) {
		String trace = "\nvvvvvvvvvvvvvvFULL STACK TRACEvvvvvvvvvvvvvv";
		trace = trace + "\n" + e.getClass().getName() + ":" + e.getMessage();
		for (final StackTraceElement element: e.getStackTrace()) {
			trace = trace + "\n\t" + element;
		}
		while (e.getCause() != null) {
			e = e.getCause();
			trace = trace + "\nCaused by:" + e.getClass().getName() + ":" + e.getMessage();
			for (final StackTraceElement element: e.getStackTrace()) {
				trace = trace + "\n\t" + element;
			}
		}
		trace = trace + "\n^^^^^^^^^^^^^^^FULL STACK TRACE^^^^^^^^^^^^^^^";
		return trace;
	}

}
