var express = require('express');
var app = express();
var server = require('http').Server(app);
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}) );

app.use( express.static(__dirname + '/WebContent' ) );

var listener = server.listen(process.env.PORT || 5000, function(){
    console.log('Listening on port ' + listener.address().port); //Listening on port 5000
});

app.get('/', function(req, res){
	  res.sendFile(__dirname + '/WebContent/index.html');
	});
app.get('/login', function(req, res){
	  res.sendFile(__dirname + '/WebContent/index.html');
	});
app.get('/home', function(req, res){
	  res.sendFile(__dirname + '/WebContent/index.html');
	});
app.get('/current-location', function(req, res){
	  res.sendFile(__dirname + '/WebContent/index.html');
	});
app.get('/previous-tracks', function(req, res){
	  res.sendFile(__dirname + '/WebContent/index.html');
	});
app.get('/friends-location', function(req, res){
	  res.sendFile(__dirname + '/WebContent/index.html');
	});
app.get('/personal-settings', function(req, res){
	  res.sendFile(__dirname + '/WebContent/index.html');
	});