"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var app_component_1 = require('./components/app.component');
var app_routing_1 = require('./routes/app.routing');
var login_component_1 = require('./components/login.component');
var register_component_1 = require('./components/register.component');
var home_component_1 = require('./components/home.component');
var current_location_component_1 = require('./components/current-location.component');
var previous_tracks_component_1 = require('./components/previous-tracks.component');
var friends_location_component_1 = require('./components/friends-location.component');
var navigation_bar_component_1 = require('./components/navigation-bar.component');
var google_map_component_1 = require('./components/google-map.component');
var routes_list_component_1 = require('./components/routes-list.component');
var friends_list_component_1 = require('./components/friends-list.component');
var own_friends_list_component_1 = require('./components/own-friends-list.component');
var personal_settings_component_1 = require('./components/personal-settings.component');
var authentication_service_1 = require('./services/authentication.service');
var footprint_service_1 = require('./services/footprint.service');
var route_service_1 = require('./services/route.service');
var track_user_service_1 = require('./services/track-user.service');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                app_routing_1.routing,
                http_1.HttpModule
            ],
            declarations: [
                app_component_1.AppComponent,
                login_component_1.LoginComponent,
                register_component_1.RegisterComponent,
                navigation_bar_component_1.NavigationBarComponent,
                home_component_1.HomeComponent,
                current_location_component_1.CurrentLocationComponent,
                previous_tracks_component_1.PreviousTracksComponent,
                google_map_component_1.GoogleMapComponent,
                routes_list_component_1.RoutesListComponent,
                friends_location_component_1.FriendsLocationComponent,
                friends_list_component_1.FriendsListComponent,
                own_friends_list_component_1.OwnFriendsListComponent,
                personal_settings_component_1.PersonalSettingsComponent,
            ],
            providers: [
                app_routing_1.appRoutingProviders,
                authentication_service_1.AuthenticationService,
                footprint_service_1.FootprintService,
                route_service_1.RouteService,
                track_user_service_1.TrackUserService
            ],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map