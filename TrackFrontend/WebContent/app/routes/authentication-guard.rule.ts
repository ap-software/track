import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class AuthenticationGuardRule {
  constructor(private router: Router, private authenticationService: AuthenticationService) {}
  
  public canActivate(): Observable<boolean> {
	  return this.authenticationService.check()
      .map((result) => {
				if (result) {
					return true;
				} else {
					this.router.navigate(['/login']);
					console.log('Niezalogowany. Przekierowanie do loginu');
					return false;
				}
			});  
  }
}