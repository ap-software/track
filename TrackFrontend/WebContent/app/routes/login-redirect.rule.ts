import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class LoginRedirectRule {
  constructor(private router: Router, private authenticationService: AuthenticationService) {}
  
  public canActivate(): Observable<boolean> {
	  return this.authenticationService.check()
      .map((result) => {
				if (!result) {
					console.log('Niezalogowany. Moze wejsc do /login');
					return true;
				} else {
					this.router.navigate(['/home']);
					console.log('Zalogowany. Przekierowanie do home');
					return false;
				}
			});  
  }
}