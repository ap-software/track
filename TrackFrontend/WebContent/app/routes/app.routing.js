"use strict";
var router_1 = require('@angular/router');
var login_component_1 = require('../components/login.component');
var home_component_1 = require('../components/home.component');
var current_location_component_1 = require('../components/current-location.component');
var previous_tracks_component_1 = require('../components/previous-tracks.component');
var friends_location_component_1 = require('../components/friends-location.component');
var personal_settings_component_1 = require('../components/personal-settings.component');
var authentication_guard_rule_1 = require('./authentication-guard.rule');
var login_redirect_rule_1 = require('./login-redirect.rule');
var appRoutes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: login_component_1.LoginComponent, canActivate: [login_redirect_rule_1.LoginRedirectRule] },
    { path: 'home', component: home_component_1.HomeComponent, canActivate: [authentication_guard_rule_1.AuthenticationGuardRule] },
    { path: 'current-location', component: current_location_component_1.CurrentLocationComponent, canActivate: [authentication_guard_rule_1.AuthenticationGuardRule] },
    { path: 'previous-tracks', component: previous_tracks_component_1.PreviousTracksComponent, canActivate: [authentication_guard_rule_1.AuthenticationGuardRule] },
    { path: 'friends-location', component: friends_location_component_1.FriendsLocationComponent, canActivate: [authentication_guard_rule_1.AuthenticationGuardRule] },
    { path: 'personal-settings', component: personal_settings_component_1.PersonalSettingsComponent, canActivate: [authentication_guard_rule_1.AuthenticationGuardRule] }
];
exports.appRoutingProviders = [
    login_redirect_rule_1.LoginRedirectRule,
    authentication_guard_rule_1.AuthenticationGuardRule
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map