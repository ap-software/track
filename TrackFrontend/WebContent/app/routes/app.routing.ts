import {Routes, RouterModule} from '@angular/router'; 
import {LoginComponent} from '../components/login.component'; 
import {HomeComponent} from '../components/home.component';
import {CurrentLocationComponent} from '../components/current-location.component';
import {PreviousTracksComponent} from '../components/previous-tracks.component';
import {FriendsLocationComponent} from '../components/friends-location.component';
import {PersonalSettingsComponent} from '../components/personal-settings.component';

import {AuthenticationGuardRule} from './authentication-guard.rule';
import {LoginRedirectRule} from './login-redirect.rule';


const appRoutes: Routes = [
	{path: '', redirectTo: '/login', pathMatch: 'full'},
	{path: 'login', component: LoginComponent, canActivate: [LoginRedirectRule]},
	{path: 'home', component: HomeComponent, canActivate: [AuthenticationGuardRule]},
	{path: 'current-location', component: CurrentLocationComponent, canActivate: [AuthenticationGuardRule]},
	{path: 'previous-tracks', component: PreviousTracksComponent, canActivate: [AuthenticationGuardRule]},
	{path: 'friends-location', component: FriendsLocationComponent, canActivate: [AuthenticationGuardRule]},
	{path: 'personal-settings', component: PersonalSettingsComponent, canActivate: [AuthenticationGuardRule]}
];


export const appRoutingProviders: any[] = [
	LoginRedirectRule,
	AuthenticationGuardRule
];

export const routing = RouterModule.forRoot(appRoutes);
