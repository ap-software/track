import {Component} from '@angular/core';
import {TrackUser} from '../utilities/track-user';
import {Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {JsonWebToken} from '../utilities/json-web-token';

@Component({
	selector: 'register',
	templateUrl: 'app/components/register.component.html',
	styleUrls: ['app/components/register.component.css'],
})

export class RegisterComponent{
	private loading: boolean = false;
	private errorMessage: string = '';
	private active: boolean = true;
	//private submitted: boolean = false;
	constructor(
		private router: Router,
		private authenticationService: AuthenticationService
	){}
		
	private model = new TrackUser('','','','','','','');
	private doRegister(){
		
		console.log(this.diagnostic);
		
		this.loading=true;
		this.errorMessage='';
		this.formReset();
		this.authenticationService.register(this.model)
					.subscribe(
						message=>this.onSuccessRegister(message),
						error => this.onErrorRegister(error),
						()=>this.onCompleteRegister()
						);
		//this.submitted=true;
	}
	
	private formReset(){
		this.active=false;
		setTimeout(()=>this.active=true,0);
	}
	
	private onSuccessRegister(message:string){
		this.errorMessage=message;
		console.log(this.diagnostic);
		this.loading=false;
	}
	
	private onErrorRegister(error:any){
		this.errorMessage=error;
		console.log(this.diagnostic);
		if(this.errorMessage=='undefined -  undefined'){
			this.errorMessage='Cannot connect to server';
		}
		this.loading=false;
	}
	
	private onCompleteRegister(){
		this.loading=false;
		this.router.navigate(['/home']);
	}
	
	public get diagnostic(){
		return JSON.stringify(this.model)
		//+'\n'+JSON.stringify(this.jwt)
		+'\n'+JSON.stringify(this.errorMessage);}
}