"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _this = this;
var core_1 = require("@angular/core");
require('rxjs/add/operator/do');
require('rxjs/add/operator/delay');
var Observable_1 = require('rxjs/Observable');
var ng2_pagination_1 = require('ng2-pagination');
var event_service_1 = require('../services/event.service');
function generateTracks() { for (var meals = [], dishes = ["noodles", "sausage", "beans on toast", "cheeseburger", "battered mars bar", "crisp butty", "yorkshire pudding", "wiener schnitzel", "sauerkraut mit ei", "salad", "onion soup", "bak choi", "avacado maki"], sides = ["with chips", "a la king", "drizzled with cheese sauce", "with a side salad", "on toast", "with ketchup", "on a bed of cabbage", "wrapped in streaky bacon", "on a stick with cheese", "in pitta bread"], i = 1; 100 >= i; i++) {
    var dish = dishes[Math.floor(Math.random() * dishes.length)], side = sides[Math.floor(Math.random() * sides.length)];
    meals.push("track " + i + ": " + dish + " " + side);
} return meals; }
var EventsListComponent = (function () {
    function EventsListComponent() {
        this.eventsList = generateTracks();
        this.p = 1;
    }
    __decorate([
        core_1.Input('data'), 
        __metadata('design:type', Array)
    ], EventsListComponent.prototype, "eventsList", void 0);
    EventsListComponent = __decorate([
        core_1.Component({
            selector: 'events-list',
            templateUrl: 'app/components/events-list.component.html',
            styleUrls: ['app/components/events-list.component.css'],
            directives: [ng2_pagination_1.PaginationControlsCmp],
            pipes: [ng2_pagination_1.PaginatePipe],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush,
            providers: [ng2_pagination_1.PaginationService]
        }), 
        __metadata('design:paramtypes', [])
    ], EventsListComponent);
    return EventsListComponent;
}());
exports.EventsListComponent = EventsListComponent;
this.errorMessage;
string = '';
loading: boolean;
constructor(private, eventService, event_service_1.EventService);
{ }
ngOnInit();
{
    this.getPage(1);
}
getPage(page, number);
{
    this.loading = true;
    this.asyncTracksList = serverCall(this.eventsList, page)
        .do(function (res) {
        _this.total = res.total;
        _this.p = page;
        _this.loading = false;
    })
        .map(function (res) { return res.items; });
}
onSuccessGetEventsList(events, any);
{
    this.errorMessage = "Success";
    this.eventsList = events.map(function (event) {
        return { id: event.id,
            name: event.name,
            eventCreationTime: new Date(event.eventCreationTime) };
    });
    console.log(JSON.stringify(this.eventsList[0].eventCreationTime.toString()));
}
onErrorGetEventsList(error, any);
{
    this.errorMessage = error;
    if (this.errorMessage == 'undefined -  undefined') {
        this.errorMessage = 'Cannot connect to server';
    }
}
onCompleteGetEventsList();
{
    console.log("Eventy z serwera:" + JSON.stringify(this.eventsList));
    console.log(JSON.stringify("DATA:" + new Date()));
    var cos = this.eventsList[0].eventCreationTime;
    console.log(JSON.stringify("COS:" + cos));
    console.log(JSON.stringify("DATA:" + this.eventsList[0].eventCreationTime));
    var date = new Date(cos);
    console.log(JSON.stringify("DATA NORM:" + date));
}
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return Observable_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
//# sourceMappingURL=tracks-list.component.js.map