"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/do');
require('rxjs/add/operator/delay');
var ng2_pagination_1 = require('ng2-pagination');
var route_service_1 = require('../services/route.service');
function generateTracks() { for (var meals = [], dishes = ["noodles", "sausage", "beans on toast", "cheeseburger", "battered mars bar", "crisp butty", "yorkshire pudding", "wiener schnitzel", "sauerkraut mit ei", "salad", "onion soup", "bak choi", "avacado maki"], sides = ["with chips", "a la king", "drizzled with cheese sauce", "with a side salad", "on toast", "with ketchup", "on a bed of cabbage", "wrapped in streaky bacon", "on a stick with cheese", "in pitta bread"], i = 1; 2 >= i; i++) {
    var dish = dishes[Math.floor(Math.random() * dishes.length)], side = sides[Math.floor(Math.random() * sides.length)];
    meals.push("track " + i + ": " + dish + " " + side);
} return meals; }
var RoutesListComponent = (function () {
    function RoutesListComponent(routeService) {
        this.routeService = routeService;
        this.sendRouteToParentEvent = new core_1.EventEmitter();
        this.page = 1;
        this.errorMessage = '';
    }
    RoutesListComponent.prototype.ngOnInit = function () {
        this.getPage(1);
    };
    RoutesListComponent.prototype.sendRouteToParent = function (route) {
        console.log('Chosen route in RoutesListComponent' + JSON.stringify(route));
        this.sendRouteToParentEvent.emit(route);
    };
    RoutesListComponent.prototype.getRouteLabel = function (route) {
        var dataUTC = new Date(route.routeCreationTime);
        return dataUTC.toUTCString() + ' ' + route.name;
    };
    RoutesListComponent.prototype.getPage = function (page) {
        var _this = this;
        this.loading = true;
        var perPage = 8;
        var start = (page - 1) * perPage;
        var end = start + perPage;
        this.asyncRoutesLabelList = this.routeService.getRoutesList() //actually this is not really server call
            .do(function (res) {
            _this.total = res.length;
            _this.page = page;
            _this.loading = false;
        }).map(function (res) { return res.slice(start, end); });
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], RoutesListComponent.prototype, "sendRouteToParentEvent", void 0);
    RoutesListComponent = __decorate([
        core_1.Component({
            selector: 'routes-list',
            templateUrl: 'app/components/routes-list.component.html',
            styleUrls: ['app/components/routes-list.component.css'],
            directives: [ng2_pagination_1.PaginationControlsCmp],
            pipes: [ng2_pagination_1.PaginatePipe],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush,
            providers: [ng2_pagination_1.PaginationService]
        }), 
        __metadata('design:paramtypes', [route_service_1.RouteService])
    ], RoutesListComponent);
    return RoutesListComponent;
}());
exports.RoutesListComponent = RoutesListComponent;
//# sourceMappingURL=routes-list.component.js.map