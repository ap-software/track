"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/do');
require('rxjs/add/operator/delay');
var ng2_pagination_1 = require('ng2-pagination');
var track_user_service_1 = require('../services/track-user.service');
var OwnFriendsListComponent = (function () {
    function OwnFriendsListComponent(trackUserService) {
        this.trackUserService = trackUserService;
        this.page = 1;
        this.errorMessage = '';
        this.active = true;
    }
    OwnFriendsListComponent.prototype.ngOnInit = function () {
        this.getPage(1);
    };
    OwnFriendsListComponent.prototype.formReset = function () {
        var _this = this;
        this.active = false;
        setTimeout(function () { return _this.active = true; }, 0);
    };
    OwnFriendsListComponent.prototype.doAddOwnFriend = function () {
        var _this = this;
        this.loading = true;
        //this.formReset();
        this.trackUserService.addOwnFriend(this.newOwnFriend)
            .subscribe(function (message) { return _this.onSuccessAddOwnFriend(message); }, function (error) { return _this.onErrorAddOwnFriend(error); }, function () { return _this.onCompleteAddOwnFriend(); });
    };
    OwnFriendsListComponent.prototype.onSuccessAddOwnFriend = function (message) {
        this.loading = false;
        this.errorMessage = message;
    };
    OwnFriendsListComponent.prototype.onErrorAddOwnFriend = function (error) {
        this.errorMessage = error;
        if (this.errorMessage == 'undefined -  undefined') {
            this.errorMessage = 'Cannot connect to server';
        }
        this.loading = false;
    };
    OwnFriendsListComponent.prototype.onCompleteAddOwnFriend = function () {
        this.getPage(1);
        this.loading = false;
    };
    OwnFriendsListComponent.prototype.doRemoveOwnFriend = function (trackUser) {
        var _this = this;
        this.loading = true;
        this.trackUserService.removeOwnFriend(trackUser)
            .subscribe(function (message) { return _this.onSuccessRemoveOwnFriend(message); }, function (error) { return _this.onErrorRemoveOwnFriend(error); }, function () { return _this.onCompleteRemoveOwnFriend(); });
    };
    OwnFriendsListComponent.prototype.onSuccessRemoveOwnFriend = function (message) {
        this.loading = false;
        this.errorMessage = message;
    };
    OwnFriendsListComponent.prototype.onErrorRemoveOwnFriend = function (error) {
        this.errorMessage = error;
        if (this.errorMessage == 'undefined -  undefined') {
            this.errorMessage = 'Cannot connect to server';
        }
        this.loading = false;
    };
    OwnFriendsListComponent.prototype.onCompleteRemoveOwnFriend = function () {
        this.getPage(1);
        this.loading = false;
    };
    OwnFriendsListComponent.prototype.getTrackUserLabel = function (trackUser) {
        return trackUser.username;
    };
    OwnFriendsListComponent.prototype.getPage = function (page) {
        var _this = this;
        this.loading = true;
        var perPage = 10;
        var start = (page - 1) * perPage;
        var end = start + perPage;
        this.asyncOwnFriendsList = this.trackUserService.getFriendsList() //actually this is not really server call
            .do(function (res) {
            _this.total = res.length;
            _this.page = page;
            _this.loading = false;
        }).map(function (res) { return res.slice(start, end); });
    };
    OwnFriendsListComponent = __decorate([
        core_1.Component({
            selector: 'own-friends-list',
            templateUrl: 'app/components/own-friends-list.component.html',
            styleUrls: ['app/components/own-friends-list.component.css'],
            directives: [ng2_pagination_1.PaginationControlsCmp],
            pipes: [ng2_pagination_1.PaginatePipe],
            //changeDetection: ChangeDetectionStrategy.OnPush,
            providers: [ng2_pagination_1.PaginationService]
        }), 
        __metadata('design:paramtypes', [track_user_service_1.TrackUserService])
    ], OwnFriendsListComponent);
    return OwnFriendsListComponent;
}());
exports.OwnFriendsListComponent = OwnFriendsListComponent;
//# sourceMappingURL=own-friends-list.component.js.map