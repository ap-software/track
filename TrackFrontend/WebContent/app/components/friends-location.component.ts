import {Component,OnInit} from '@angular/core';
import {NavigationBarComponent} from '../components/navigation-bar.component';
import {GoogleMapComponent} from '../components/google-map.component';
import {RoutesListComponent} from '../components/routes-list.component';

import {AuthenticationService} from '../services/authentication.service';
import {FootprintService} from '../services/footprint.service';
import {TrackUserService} from '../services/track-user.service';

import {Constants} from '../utilities/constants';
import {TrackUser} from '../utilities/track-user';


@Component({
	selector: 'friends-location',
	templateUrl: 'app/components/friends-location.component.html',
	styleUrls : ['app/components/friends-location.component.css'],
})	
export class FriendsLocationComponent{
  
  private static get TIME_SHIFT():number{ return (-1)*20*1000;}
	
	private googleMapComponent:GoogleMapComponent;
	//private routesList:any;//Event[];
	private errorMessage:string='';
	private chosenTrackUser:any = null;
	private trackLatLng = null;//:google.maps.LatLng[];
	private loading: boolean = false;
  private date:Date = null;
  
  private dateLastKnownLocation:Date;
	
	constructor(
		private authenticationService:AuthenticationService,
		private footprintService:FootprintService,
		private trackUserService:TrackUserService
	) {}
	
  public ngOnInit(){
    let currentDate:Date=new Date();
    this.date= new Date(currentDate.getTime()+ FriendsLocationComponent.TIME_SHIFT);
  }
  
  //GoogleMapComponent send event after google map initialization
  private catchMapFromChild(googleMapComponent:GoogleMapComponent):void {
		this.googleMapComponent=googleMapComponent;
	}
	
	private catchTrackUserFromList(trackUser:TrackUser):void {
		this.chosenTrackUser=trackUser;
		this.getTrackOfFriend(this.chosenTrackUser);
	}
	
	public getTrackOfFriend(trackUser:TrackUser){
    this.errorMessage="Requesting for friend location...";
		this.loading=true;
		this.footprintService.getFriendCurrentFootprints(trackUser,this.date)
				.subscribe(
						points=>this.onSuccessGetRouteFootprints(points),
						error => this.onErrorGetRouteFootprints(error),
						()=>this.onCompleteGetRouteFootprints()
						);
	}
	
	private onSuccessGetRouteFootprints(footprints:any){
		this.errorMessage="Data successfully received";
		if(footprints.length!=0){
			this.trackLatLng = footprints.list.map((footprint)=>{return {lat: footprint.latitude, lng: footprint.longitude};});
			console.log(JSON.stringify(this.trackLatLng));
      if(footprints.list!=null){
        if(footprints.list[0]!=null){
          this.dateLastKnownLocation=new Date(footprints.list[footprints.list.length-1].footprintCreationTime);
          return;
        }
      }
    this.dateLastKnownLocation = null;  
		}
    else{
			this.trackLatLng=undefined;
      this.dateLastKnownLocation = null; 
		}
	}
	
	private onErrorGetRouteFootprints(error:any){
		this.errorMessage=error;
		if(this.errorMessage=='undefined -  undefined'){
			this.errorMessage='Cannot connect to server';
		}
		this.loading=false;
	}
	
	private onCompleteGetRouteFootprints(){
		if(this.trackLatLng!=null && this.trackLatLng.length>0){
			this.googleMapComponent.drawTrackOfRoute(this.trackLatLng, true);
		}
		if(this.dateLastKnownLocation!=null){
      this.errorMessage="Last known location on "
        +Constants.daysOfWeek[this.dateLastKnownLocation.getDay()]+", "
        +this.dateLastKnownLocation.toLocaleDateString()+" "
        +this.dateLastKnownLocation.toLocaleTimeString();
    }
    else{
      this.errorMessage="There isn't any \""+this.chosenTrackUser.username+"\""+ " location in database";
      this.googleMapComponent.clearMap();
    }
    this.loading=false;    
		
	}
	
}