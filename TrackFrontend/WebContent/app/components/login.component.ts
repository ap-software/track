import {Component} from '@angular/core';
import {TrackUser} from '../utilities/track-user';
import {Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {JsonWebToken} from '../utilities/json-web-token';

@Component({
	selector: 'login',
	templateUrl: 'app/components/login.component.html',
	styleUrls: ['app/components/login.component.css'],
})

export class LoginComponent{
	private loading: boolean = false;
	private active: boolean=true;
	private visibleRegister: boolean=false;
	//private submitted: boolean = false;
	private errorMessage: string = '';
	constructor(
		private router: Router,
		private authenticationService: AuthenticationService
	){}
	
	private model = new TrackUser('','','','','','','');

	private doLogin(){
		this.loading=true;
		this.errorMessage='';
		this.formReset();
		this.authenticationService.login(this.model.accessorUsername,this.model.accessorPassword)
					.subscribe(
						jwt=>this.onSuccessLogin(jwt),
						error => this.onErrorLogin(error),
						()=>this.onCompleteLogin()
						);
		//this.submitted=true;
	}
	
	private toogleVisibleRegister(){
		this.visibleRegister=!this.visibleRegister;
	}
	
	private formReset(){
		this.active=false;
		setTimeout(()=>this.active=true,0);
	}
	
	private onSuccessLogin(jwt:JsonWebToken){
		this.errorMessage="Success logged in";
		console.log(this.diagnostic);
		this.loading=false;
	}
	
	private onErrorLogin(error:any){
		this.errorMessage=error;
		console.log(this.diagnostic);
		if(this.errorMessage=='undefined -  undefined'){
			this.errorMessage='Cannot connect to server';
		}
		this.loading=false;
	}
	
	private onCompleteLogin(){
		this.loading=false;
		this.router.navigate(['/home']);
	}
	
	public get diagnostic(){
		return JSON.stringify(this.model)
		//+'\n'+JSON.stringify(this.jwt)
		+'\n'+JSON.stringify(this.errorMessage);}
}