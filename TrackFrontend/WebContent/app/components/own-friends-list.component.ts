import {ChangeDetectionStrategy, Component, Input, Output, EventEmitter} from "@angular/core";

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import {Observable} from 'rxjs/Observable';
import {PaginatePipe, PaginationControlsCmp, PaginationService} from 'ng2-pagination';

import {TrackUserService} from '../services/track-user.service';
import {TrackUser} from '../utilities/track-user';

@Component({
	selector: 'own-friends-list',
	templateUrl: 'app/components/own-friends-list.component.html',
	styleUrls: ['app/components/own-friends-list.component.css'],
	directives: [PaginationControlsCmp],
	pipes: [PaginatePipe],
	//changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [PaginationService]
})	
export class OwnFriendsListComponent{
	//@Output() private sendTrackUserToParentEvent:EventEmitter<TrackUser> = new EventEmitter<TrackUser>();
	//sendTrackUserToParent(user:TrackUser){
	//	this.sendTrackUserToParentEvent.emit(user);
	//}	
	private asyncOwnFriendsList: Observable<TrackUser[]>;
	private total: number;
	private newOwnFriend:string;
	
	private page:number=1;
	private errorMessage:string='';
	private loading: boolean;
	private active: boolean=true;
	
	constructor(
		private trackUserService:TrackUserService
	){}

	private ngOnInit() {
		this.getPage(1);
	}
	
	private formReset(){
		this.active=false;
		setTimeout(()=>this.active=true,0);
	}
	
	private doAddOwnFriend(){
		this.loading=true;
		//this.formReset();
		this.trackUserService.addOwnFriend(this.newOwnFriend)
				.subscribe(
							message=>this.onSuccessAddOwnFriend(message),
							error => this.onErrorAddOwnFriend(error),
							()=>this.onCompleteAddOwnFriend()
							);
	}
	
	private onSuccessAddOwnFriend(message:any){
		this.loading=false;
		this.errorMessage=message;
	}
	
	private onErrorAddOwnFriend(error:any){
		this.errorMessage=error;
		if(this.errorMessage=='undefined -  undefined'){
			this.errorMessage='Cannot connect to server';
		}
		this.loading=false;
	}
	
	private onCompleteAddOwnFriend(){
		this.getPage(1);
		this.loading=false;
	}
	
	private doRemoveOwnFriend(trackUser:TrackUser){
		this.loading=true;
		this.trackUserService.removeOwnFriend(trackUser)
				.subscribe(
							message=>this.onSuccessRemoveOwnFriend(message),
							error => this.onErrorRemoveOwnFriend(error),
							()=>this.onCompleteRemoveOwnFriend()
							);
	}
	
	private onSuccessRemoveOwnFriend(message:any){
		this.loading=false;
		this.errorMessage=message;
	}
	
	private onErrorRemoveOwnFriend(error:any){
		this.errorMessage=error;
		if(this.errorMessage=='undefined -  undefined'){
			this.errorMessage='Cannot connect to server';
		}
		this.loading=false;
	}
	
	private onCompleteRemoveOwnFriend(){
		this.getPage(1);
		this.loading=false;
	}
	
	private getTrackUserLabel(trackUser:any):string{
		return trackUser.username;
	}
	
	private getPage(page: number) {
		this.loading = true;
		const perPage:number = 10;
		const start = (page - 1) * perPage;
    	const end = start + perPage;
		
		this.asyncOwnFriendsList = this.trackUserService.getFriendsList()//actually this is not really server call
			.do(res => {
				this.total = res.length;
				this.page = page;
				this.loading = false;
			}).map(res => res.slice(start,end));
	}
}