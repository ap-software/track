"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var authentication_service_1 = require('../services/authentication.service');
var footprint_service_1 = require('../services/footprint.service');
var track_user_service_1 = require('../services/track-user.service');
var constants_1 = require('../utilities/constants');
var FriendsLocationComponent = (function () {
    function FriendsLocationComponent(authenticationService, footprintService, trackUserService) {
        this.authenticationService = authenticationService;
        this.footprintService = footprintService;
        this.trackUserService = trackUserService;
        //private routesList:any;//Event[];
        this.errorMessage = '';
        this.chosenTrackUser = null;
        this.trackLatLng = null; //:google.maps.LatLng[];
        this.loading = false;
        this.date = null;
    }
    Object.defineProperty(FriendsLocationComponent, "TIME_SHIFT", {
        get: function () { return (-1) * 20 * 1000; },
        enumerable: true,
        configurable: true
    });
    FriendsLocationComponent.prototype.ngOnInit = function () {
        var currentDate = new Date();
        this.date = new Date(currentDate.getTime() + FriendsLocationComponent.TIME_SHIFT);
    };
    //GoogleMapComponent send event after google map initialization
    FriendsLocationComponent.prototype.catchMapFromChild = function (googleMapComponent) {
        this.googleMapComponent = googleMapComponent;
    };
    FriendsLocationComponent.prototype.catchTrackUserFromList = function (trackUser) {
        this.chosenTrackUser = trackUser;
        this.getTrackOfFriend(this.chosenTrackUser);
    };
    FriendsLocationComponent.prototype.getTrackOfFriend = function (trackUser) {
        var _this = this;
        this.errorMessage = "Requesting for friend location...";
        this.loading = true;
        this.footprintService.getFriendCurrentFootprints(trackUser, this.date)
            .subscribe(function (points) { return _this.onSuccessGetRouteFootprints(points); }, function (error) { return _this.onErrorGetRouteFootprints(error); }, function () { return _this.onCompleteGetRouteFootprints(); });
    };
    FriendsLocationComponent.prototype.onSuccessGetRouteFootprints = function (footprints) {
        this.errorMessage = "Data successfully received";
        if (footprints.length != 0) {
            this.trackLatLng = footprints.list.map(function (footprint) { return { lat: footprint.latitude, lng: footprint.longitude }; });
            console.log(JSON.stringify(this.trackLatLng));
            if (footprints.list != null) {
                if (footprints.list[0] != null) {
                    this.dateLastKnownLocation = new Date(footprints.list[footprints.list.length - 1].footprintCreationTime);
                    return;
                }
            }
            this.dateLastKnownLocation = null;
        }
        else {
            this.trackLatLng = undefined;
            this.dateLastKnownLocation = null;
        }
    };
    FriendsLocationComponent.prototype.onErrorGetRouteFootprints = function (error) {
        this.errorMessage = error;
        if (this.errorMessage == 'undefined -  undefined') {
            this.errorMessage = 'Cannot connect to server';
        }
        this.loading = false;
    };
    FriendsLocationComponent.prototype.onCompleteGetRouteFootprints = function () {
        if (this.trackLatLng != null && this.trackLatLng.length > 0) {
            this.googleMapComponent.drawTrackOfRoute(this.trackLatLng, true);
        }
        if (this.dateLastKnownLocation != null) {
            this.errorMessage = "Last known location on "
                + constants_1.Constants.daysOfWeek[this.dateLastKnownLocation.getDay()] + ", "
                + this.dateLastKnownLocation.toLocaleDateString() + " "
                + this.dateLastKnownLocation.toLocaleTimeString();
        }
        else {
            this.errorMessage = "There isn't any \"" + this.chosenTrackUser.username + "\"" + " location in database";
            this.googleMapComponent.clearMap();
        }
        this.loading = false;
    };
    FriendsLocationComponent = __decorate([
        core_1.Component({
            selector: 'friends-location',
            templateUrl: 'app/components/friends-location.component.html',
            styleUrls: ['app/components/friends-location.component.css'],
        }), 
        __metadata('design:paramtypes', [authentication_service_1.AuthenticationService, footprint_service_1.FootprintService, track_user_service_1.TrackUserService])
    ], FriendsLocationComponent);
    return FriendsLocationComponent;
}());
exports.FriendsLocationComponent = FriendsLocationComponent;
//# sourceMappingURL=friends-location.component.js.map