"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var track_user_1 = require('../utilities/track-user');
var router_1 = require('@angular/router');
var authentication_service_1 = require('../services/authentication.service');
var LoginComponent = (function () {
    function LoginComponent(router, authenticationService) {
        this.router = router;
        this.authenticationService = authenticationService;
        this.loading = false;
        this.active = true;
        this.visibleRegister = false;
        //private submitted: boolean = false;
        this.errorMessage = '';
        this.model = new track_user_1.TrackUser('', '', '', '', '', '', '');
    }
    LoginComponent.prototype.doLogin = function () {
        var _this = this;
        this.loading = true;
        this.errorMessage = '';
        this.formReset();
        this.authenticationService.login(this.model.accessorUsername, this.model.accessorPassword)
            .subscribe(function (jwt) { return _this.onSuccessLogin(jwt); }, function (error) { return _this.onErrorLogin(error); }, function () { return _this.onCompleteLogin(); });
        //this.submitted=true;
    };
    LoginComponent.prototype.toogleVisibleRegister = function () {
        this.visibleRegister = !this.visibleRegister;
    };
    LoginComponent.prototype.formReset = function () {
        var _this = this;
        this.active = false;
        setTimeout(function () { return _this.active = true; }, 0);
    };
    LoginComponent.prototype.onSuccessLogin = function (jwt) {
        this.errorMessage = "Success logged in";
        console.log(this.diagnostic);
        this.loading = false;
    };
    LoginComponent.prototype.onErrorLogin = function (error) {
        this.errorMessage = error;
        console.log(this.diagnostic);
        if (this.errorMessage == 'undefined -  undefined') {
            this.errorMessage = 'Cannot connect to server';
        }
        this.loading = false;
    };
    LoginComponent.prototype.onCompleteLogin = function () {
        this.loading = false;
        this.router.navigate(['/home']);
    };
    Object.defineProperty(LoginComponent.prototype, "diagnostic", {
        get: function () {
            return JSON.stringify(this.model)
                + '\n' + JSON.stringify(this.errorMessage);
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'login',
            templateUrl: 'app/components/login.component.html',
            styleUrls: ['app/components/login.component.css'],
        }), 
        __metadata('design:paramtypes', [router_1.Router, authentication_service_1.AuthenticationService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map