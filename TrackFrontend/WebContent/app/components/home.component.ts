import {Component} from '@angular/core';
import {NavigationBarComponent} from '../components/navigation-bar.component';



@Component({
	selector: 'home',
	templateUrl: 'app/components/home.component.html',
  styleUrls: ['app/components/home.component.css'],
	//directives:[NavigationBarComponent]
})
export class HomeComponent{}