"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var track_user_1 = require('../utilities/track-user');
var authentication_service_1 = require('../services/authentication.service');
var track_user_service_1 = require('../services/track-user.service');
var PersonalSettingsComponent = (function () {
    function PersonalSettingsComponent(authenticationService, trackUserService) {
        this.authenticationService = authenticationService;
        this.trackUserService = trackUserService;
        this.loadingChangePassword = false;
        this.loadingChangePersonalData = false;
        this.errorMessageChangePassword = '';
        this.errorMessageChangePersonalData = '';
        this.activeChangePassword = true;
        this.activeChangePersonalData = true;
        //private submitted: boolean = false;
        this.model = new track_user_1.TrackUser('', '', '', '', '', '', '');
    }
    PersonalSettingsComponent.prototype.ngAfterViewInit = function () {
        //this.loadingChangePersonalData=true;
        this.getPersonalData();
    };
    PersonalSettingsComponent.prototype.doChangePersonalData = function () {
        this.formChangePersonalDataReset();
        this.setPersonalData();
    };
    PersonalSettingsComponent.prototype.doChangePassword = function () {
        this.formChangePasswordReset();
        this.setPassword();
    };
    PersonalSettingsComponent.prototype.getPersonalData = function () {
        var _this = this;
        this.loadingChangePersonalData = true;
        this.trackUserService.getPersonalData()
            .subscribe(function (personalData) { return _this.onSuccessGetPersonalData(personalData); }, function (error) { return _this.onErrorGetPersonalData(error); }, function () { return _this.onCompleteGetPersonalData(); });
    };
    PersonalSettingsComponent.prototype.onSuccessGetPersonalData = function (personalData) {
        this.errorMessageChangePersonalData = "";
        this.model.accessorUsername = personalData.username;
        //this.model.accessorPassword('');
        //this.model.accessorRepeatPassword('');
        this.model.accessorEmail = personalData.email;
        this.model.accessorPhone = personalData.phone;
        this.model.accessorName = personalData.name;
        this.model.accessorSurname = personalData.surname;
    };
    PersonalSettingsComponent.prototype.onErrorGetPersonalData = function (error) {
        this.errorMessageChangePersonalData = error;
        if (this.errorMessageChangePersonalData == 'undefined -  undefined') {
            this.errorMessageChangePersonalData = 'Cannot connect to server';
        }
        this.loadingChangePersonalData = false;
    };
    PersonalSettingsComponent.prototype.onCompleteGetPersonalData = function () {
        this.loadingChangePersonalData = false;
    };
    PersonalSettingsComponent.prototype.setPersonalData = function () {
        var _this = this;
        this.loadingChangePersonalData = true;
        this.trackUserService.setPersonalData(this.model)
            .subscribe(function (message) { return _this.onSuccessSetPersonalData(message); }, function (error) { return _this.onErrorSetPersonalData(error); }, function () { return _this.onCompleteSetPersonalData(); });
    };
    PersonalSettingsComponent.prototype.onSuccessSetPersonalData = function (message) {
        this.errorMessageChangePersonalData = message;
        this.loadingChangePersonalData = false;
    };
    PersonalSettingsComponent.prototype.onErrorSetPersonalData = function (error) {
        this.errorMessageChangePersonalData = error;
        console.log(this.diagnostic);
        if (this.errorMessageChangePersonalData == 'undefined -  undefined') {
            this.errorMessageChangePersonalData = 'Cannot connect to server';
        }
        this.loadingChangePersonalData = false;
    };
    PersonalSettingsComponent.prototype.onCompleteSetPersonalData = function () {
        this.loadingChangePersonalData = false;
        console.log(JSON.stringify(this.model));
    };
    PersonalSettingsComponent.prototype.setPassword = function () {
        var _this = this;
        this.loadingChangePassword = true;
        this.trackUserService.setPassword(this.model)
            .subscribe(function (message) { return _this.onSuccessSetPassword(message); }, function (error) { return _this.onErrorSetPassword(error); }, function () { return _this.onCompleteSetPassword(); });
    };
    PersonalSettingsComponent.prototype.onSuccessSetPassword = function (message) {
        this.errorMessageChangePassword = message;
        this.loadingChangePassword = false;
    };
    PersonalSettingsComponent.prototype.onErrorSetPassword = function (error) {
        this.errorMessageChangePassword = error;
        console.log(this.diagnostic);
        if (this.errorMessageChangePassword == 'undefined -  undefined') {
            this.errorMessageChangePassword = 'Cannot connect to server';
        }
        this.loadingChangePassword = false;
    };
    PersonalSettingsComponent.prototype.onCompleteSetPassword = function () {
        this.loadingChangePassword = false;
        console.log(JSON.stringify(this.model));
    };
    PersonalSettingsComponent.prototype.formChangePasswordReset = function () {
        var _this = this;
        this.activeChangePassword = false;
        setTimeout(function () { return _this.activeChangePassword = true; }, 0);
    };
    PersonalSettingsComponent.prototype.formChangePersonalDataReset = function () {
        var _this = this;
        this.activeChangePersonalData = false;
        setTimeout(function () { return _this.activeChangePersonalData = true; }, 0);
    };
    Object.defineProperty(PersonalSettingsComponent.prototype, "diagnostic", {
        get: function () {
            return JSON.stringify(this.model)
                + '\n' + JSON.stringify(this.errorMessageChangePassword)
                + '\n' + JSON.stringify(this.errorMessageChangePersonalData);
        },
        enumerable: true,
        configurable: true
    });
    PersonalSettingsComponent = __decorate([
        core_1.Component({
            selector: 'personal-settings',
            templateUrl: 'app/components/personal-settings.component.html',
            styleUrls: ['app/components/personal-settings.component.css'],
        }), 
        __metadata('design:paramtypes', [authentication_service_1.AuthenticationService, track_user_service_1.TrackUserService])
    ], PersonalSettingsComponent);
    return PersonalSettingsComponent;
}());
exports.PersonalSettingsComponent = PersonalSettingsComponent;
//# sourceMappingURL=personal-settings.component.js.map