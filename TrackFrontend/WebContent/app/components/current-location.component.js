"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var authentication_service_1 = require('../services/authentication.service');
var footprint_service_1 = require('../services/footprint.service');
var constants_1 = require('../utilities/constants');
var CurrentLocationComponent = (function () {
    function CurrentLocationComponent(authenticationService, footprintService) {
        this.authenticationService = authenticationService;
        this.footprintService = footprintService;
        this.errorMessage = '';
        this.loading = false;
        this.alreadyCentered = false;
    }
    Object.defineProperty(CurrentLocationComponent, "TIME_SHIFT", {
        get: function () { return (-1) * 20 * 1000; },
        enumerable: true,
        configurable: true
    });
    //GoogleMapComponent send event after google map initialization
    CurrentLocationComponent.prototype.catchMapFromChild = function (googleMapComponent) {
        this.googleMapComponent = googleMapComponent;
        var currentDate = new Date();
        this.beginTime = new Date(currentDate.getTime() + CurrentLocationComponent.TIME_SHIFT);
        this.startCyclicServerPooling();
    };
    CurrentLocationComponent.prototype.ngOnDestroy = function () {
        this.stopCyclicServerPooling();
    };
    CurrentLocationComponent.prototype.startCyclicServerPooling = function () {
        var _this = this;
        this.drawCurrentTrackOfUser();
        this.timerToken = setInterval(function () { return _this.drawCurrentTrackOfUser(); }, constants_1.Constants.poolingIntervalCurrentPosition);
    };
    CurrentLocationComponent.prototype.stopCyclicServerPooling = function () {
        clearTimeout(this.timerToken);
    };
    CurrentLocationComponent.prototype.onClickClearButton = function () {
        var currentDate = new Date();
        this.beginTime = new Date(currentDate.getTime() + CurrentLocationComponent.TIME_SHIFT);
    };
    CurrentLocationComponent.prototype.drawCurrentTrackOfUser = function () {
        var _this = this;
        this.loading = true;
        this.errorMessage = "Requesting for location update...";
        this.footprintService.getCurrentLocations(this.beginTime)
            .subscribe(function (points) { return _this.onSuccessGetCurrentLocations(points); }, function (error) { return _this.onErrorGetCurrentLocations(error); }, function () { return _this.onCompleteGetCurrentLocations(); });
    };
    CurrentLocationComponent.prototype.onSuccessGetCurrentLocations = function (footprints) {
        this.errorMessage = "Data successfully received";
        this.trackLatLng = footprints.list.map(function (footprint) { return { lat: footprint.latitude, lng: footprint.longitude }; });
        if (footprints.list != null) {
            if (footprints.list[0] != null) {
                this.dateLastKnownLocation = new Date(footprints.list[footprints.list.length - 1].footprintCreationTime);
                return;
            }
        }
        this.dateLastKnownLocation = null;
    };
    CurrentLocationComponent.prototype.onErrorGetCurrentLocations = function (error) {
        this.loading = false;
        this.errorMessage = error;
        if (this.errorMessage == 'undefined -  undefined') {
            this.errorMessage = 'Cannot connect to server';
        }
    };
    CurrentLocationComponent.prototype.onCompleteGetCurrentLocations = function () {
        if (this.trackLatLng != null && this.trackLatLng.length > 0) {
            if ((this.alreadyCentered == false) || (!this.googleMapComponent.isTrackInBounds())) {
                this.googleMapComponent.drawTrackOfRoute(this.trackLatLng, true);
                this.alreadyCentered = true;
            }
            else {
                this.googleMapComponent.drawTrackOfRoute(this.trackLatLng, false);
            }
        }
        if (this.dateLastKnownLocation != null) {
            this.errorMessage = "Last known location on "
                + constants_1.Constants.daysOfWeek[this.dateLastKnownLocation.getDay()] + ", "
                + this.dateLastKnownLocation.toLocaleDateString() + " "
                + this.dateLastKnownLocation.toLocaleTimeString();
        }
        else {
            this.errorMessage = "There isn't any your location in database";
            this.googleMapComponent.clearMap();
        }
        this.loading = false;
    };
    CurrentLocationComponent = __decorate([
        core_1.Component({
            selector: 'current-location',
            templateUrl: 'app/components/current-location.component.html',
            styleUrls: ['app/components/current-location.component.css'],
        }), 
        __metadata('design:paramtypes', [authentication_service_1.AuthenticationService, footprint_service_1.FootprintService])
    ], CurrentLocationComponent);
    return CurrentLocationComponent;
}());
exports.CurrentLocationComponent = CurrentLocationComponent;
//# sourceMappingURL=current-location.component.js.map