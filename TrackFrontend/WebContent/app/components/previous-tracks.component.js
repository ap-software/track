"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var authentication_service_1 = require('../services/authentication.service');
var footprint_service_1 = require('../services/footprint.service');
var route_service_1 = require('../services/route.service');
var PreviousTracksComponent = (function () {
    function PreviousTracksComponent(authenticationService, footprintService, routeService) {
        this.authenticationService = authenticationService;
        this.footprintService = footprintService;
        this.routeService = routeService;
        //private routesList:any;//Event[];
        this.errorMessage = '';
        this.chosenRoute = null;
        this.trackLatLng = null; //:google.maps.LatLng[];
        this.loading = false;
    }
    //GoogleMapComponent send event after google map initialization
    PreviousTracksComponent.prototype.catchMapFromChild = function (googleMapComponent) {
        this.googleMapComponent = googleMapComponent;
        //this.getRoutesList();
    };
    /*
    getRoutesList(){
        this.routeService.getRoutesList()
                .subscribe(
                        routes=>this.onSuccessGetRoutesList(routes),
                        error => this.onErrorGetRoutesList(error),
                        ()=>this.onCompleteGetRoutesList()
                        );
    }
    
    onSuccessGetRoutesList(routes:any){
        this.errorMessage="Success";
        this.routesList = routes.map(
                    route=>{return {id:route.id,
                            name:route.name,
                            routeCreationTime:new Date(route.routeCreationTime)};}
        );
    }
    
    onErrorGetRoutesList(error:any){
        this.errorMessage=error;
        if(this.errorMessage=='undefined -  undefined'){
            this.errorMessage='Cannot connect to server';
        }
    }
    
    onCompleteGetRoutesList(){
        //let cos:string = this.routesList[0].routeCreationTime;
        //let date:Date = new Date(cos);
    }
    */
    PreviousTracksComponent.prototype.catchRouteFromList = function (route) {
        this.chosenRoute = route;
        console.log('Chosen route in PreviousTracksComponent' + JSON.stringify(route));
        this.getTrackOfRoute(this.chosenRoute);
        /*
        console.log('Chosen route in PreviousTracksComponent'+JSON.stringify(route));
        this.footprintService.getRouteFootprints(this.chosenRoute)
                .subscribe(
                        routes=>this.onSuccessGetRoutesList(routes),
                        error => this.onErrorGetRoutesList(error),
                        ()=>this.onCompleteGetRoutesList()
                        );
        */
    };
    PreviousTracksComponent.prototype.getTrackOfRoute = function (route) {
        var _this = this;
        this.loading = true;
        this.errorMessage = "Requesting for route...";
        this.footprintService.getRouteFootprints(route)
            .subscribe(function (points) { return _this.onSuccessGetRouteFootprints(points); }, function (error) { return _this.onErrorGetRouteFootprints(error); }, function () { return _this.onCompleteGetRouteFootprints(); });
    };
    PreviousTracksComponent.prototype.onSuccessGetRouteFootprints = function (footprints) {
        this.errorMessage = "Data successfully received";
        if (footprints.length != 0) {
            this.trackLatLng = footprints.list.map(function (footprint) { return { lat: footprint.latitude, lng: footprint.longitude }; });
            console.log(JSON.stringify(this.trackLatLng));
        }
        else {
            this.trackLatLng = undefined;
        }
    };
    PreviousTracksComponent.prototype.onErrorGetRouteFootprints = function (error) {
        this.errorMessage = error;
        if (this.errorMessage == 'undefined -  undefined') {
            this.errorMessage = 'Cannot connect to server';
        }
        this.loading = false;
    };
    PreviousTracksComponent.prototype.onCompleteGetRouteFootprints = function () {
        if (this.trackLatLng != null && this.trackLatLng.length > 0) {
            this.googleMapComponent.drawTrackOfRoute(this.trackLatLng, true);
            this.errorMessage = "Chosen route \"" + this.chosenRoute.name + "\"";
        }
        else {
            this.googleMapComponent.clearMap();
            this.errorMessage = "There is not footprints for route \"" + this.chosenRoute.name + "\"";
        }
        this.loading = false;
    };
    PreviousTracksComponent = __decorate([
        core_1.Component({
            selector: 'previous-tracks',
            templateUrl: 'app/components/previous-tracks.component.html',
            styleUrls: ['app/components/previous-tracks.component.css'],
        }), 
        __metadata('design:paramtypes', [authentication_service_1.AuthenticationService, footprint_service_1.FootprintService, route_service_1.RouteService])
    ], PreviousTracksComponent);
    return PreviousTracksComponent;
}());
exports.PreviousTracksComponent = PreviousTracksComponent;
//# sourceMappingURL=previous-tracks.component.js.map