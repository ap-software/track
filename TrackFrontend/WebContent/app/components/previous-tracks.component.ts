import {Component} from '@angular/core';
import {NavigationBarComponent} from '../components/navigation-bar.component';
import {GoogleMapComponent} from '../components/google-map.component';
import {RoutesListComponent} from '../components/routes-list.component';

import {AuthenticationService} from '../services/authentication.service';
import {FootprintService} from '../services/footprint.service';
import {RouteService} from '../services/route.service';

import {Route} from '../utilities/route';


@Component({
	selector: 'previous-tracks',
	templateUrl: 'app/components/previous-tracks.component.html',
	styleUrls : ['app/components/previous-tracks.component.css'],
})	
export class PreviousTracksComponent{
	
	private googleMapComponent:GoogleMapComponent;
	//private routesList:any;//Event[];
	private errorMessage:string='';
	private chosenRoute:any = null;
	private trackLatLng = null;//:google.maps.LatLng[];
	private loading: boolean = false;
	
	constructor(
		private authenticationService:AuthenticationService,
		private footprintService:FootprintService,
		private routeService:RouteService
	) {}
	//GoogleMapComponent send event after google map initialization
	private catchMapFromChild(googleMapComponent:GoogleMapComponent):void {
		this.googleMapComponent=googleMapComponent;
		//this.getRoutesList();
	}
	/*
	getRoutesList(){
		this.routeService.getRoutesList()
				.subscribe(
						routes=>this.onSuccessGetRoutesList(routes),
						error => this.onErrorGetRoutesList(error),
						()=>this.onCompleteGetRoutesList()
			  			);
	}
	
	onSuccessGetRoutesList(routes:any){
		this.errorMessage="Success";
		this.routesList = routes.map(
					route=>{return {id:route.id,
							name:route.name,
							routeCreationTime:new Date(route.routeCreationTime)};}
		);
	}
	
	onErrorGetRoutesList(error:any){
		this.errorMessage=error;
		if(this.errorMessage=='undefined -  undefined'){
			this.errorMessage='Cannot connect to server';
		}
	}
	
	onCompleteGetRoutesList(){
		//let cos:string = this.routesList[0].routeCreationTime;
		//let date:Date = new Date(cos);
	} 
	*/
	
	private catchRouteFromList(route:Route):void {
		this.chosenRoute=route;
		console.log('Chosen route in PreviousTracksComponent'+JSON.stringify(route));
		this.getTrackOfRoute(this.chosenRoute);
		/*
		console.log('Chosen route in PreviousTracksComponent'+JSON.stringify(route));
		this.footprintService.getRouteFootprints(this.chosenRoute)
				.subscribe(
						routes=>this.onSuccessGetRoutesList(routes),
						error => this.onErrorGetRoutesList(error),
						()=>this.onCompleteGetRoutesList()
			  			);
		*/
	}
	
	public getTrackOfRoute(route:Route){
		this.loading=true;
    this.errorMessage="Requesting for route...";
		this.footprintService.getRouteFootprints(route)
				.subscribe(
						points=>this.onSuccessGetRouteFootprints(points),
						error => this.onErrorGetRouteFootprints(error),
						()=>this.onCompleteGetRouteFootprints()
						);
	}
	
	private onSuccessGetRouteFootprints(footprints:any){
		this.errorMessage="Data successfully received";
		if(footprints.length!=0){
			this.trackLatLng = footprints.list.map((footprint)=>{return {lat: footprint.latitude, lng: footprint.longitude};});
			console.log(JSON.stringify(this.trackLatLng));
		}else{
			this.trackLatLng=undefined;
		}
	}
	
	private onErrorGetRouteFootprints(error:any){
		this.errorMessage=error;
		if(this.errorMessage=='undefined -  undefined'){
			this.errorMessage='Cannot connect to server';
		}
		this.loading=false;
	}
	
	private onCompleteGetRouteFootprints(){
		if(this.trackLatLng!=null && this.trackLatLng.length>0){
			this.googleMapComponent.drawTrackOfRoute(this.trackLatLng,true);
      this.errorMessage="Chosen route \""+this.chosenRoute.name+"\"";
		}else{
      this.googleMapComponent.clearMap();
      this.errorMessage="There is not footprints for route \""+this.chosenRoute.name+"\"";
    }
		this.loading=false;
		
	}
	
}