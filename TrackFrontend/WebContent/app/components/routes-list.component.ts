import {ChangeDetectionStrategy, Component, Input, Output, EventEmitter} from "@angular/core";

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import {Observable} from 'rxjs/Observable';
import {PaginatePipe, PaginationControlsCmp, PaginationService} from 'ng2-pagination';

import {RouteService} from '../services/route.service';
import {Route} from '../utilities/route';

function generateTracks(){for(var meals=[],dishes=["noodles","sausage","beans on toast","cheeseburger","battered mars bar","crisp butty","yorkshire pudding","wiener schnitzel","sauerkraut mit ei","salad","onion soup","bak choi","avacado maki"],sides=["with chips","a la king","drizzled with cheese sauce","with a side salad","on toast","with ketchup","on a bed of cabbage","wrapped in streaky bacon","on a stick with cheese","in pitta bread"],i=1;2>=i;i++){var dish=dishes[Math.floor(Math.random()*dishes.length)],side=sides[Math.floor(Math.random()*sides.length)];meals.push("track "+i+": "+dish+" "+side)}return meals}



@Component({
	selector: 'routes-list',
	templateUrl: 'app/components/routes-list.component.html',
	styleUrls: ['app/components/routes-list.component.css'],
	directives: [PaginationControlsCmp],
	pipes: [PaginatePipe],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [PaginationService]
})	
export class RoutesListComponent{
	@Output() private sendRouteToParentEvent:EventEmitter<Route> = new EventEmitter<Route>();
	//@Input() private routesList: any[];// = generateTracks();
	private asyncRoutesLabelList: Observable<Route[]>;
	//private routesLabelList:any[];
	//p: number = 1;
	private total: number;
	
	private page:number=1;
	private errorMessage:string='';
	private loading: boolean;
	
	constructor(
		private routeService:RouteService
	){}

	public ngOnInit() {
		this.getPage(1);
	}
	
	private sendRouteToParent(route:Route){
		console.log('Chosen route in RoutesListComponent'+JSON.stringify(route));	
		this.sendRouteToParentEvent.emit(route);
	}
	
	private getRouteLabel(route:any):string{
		let dataUTC:Date = new Date(route.routeCreationTime);
		return dataUTC.toUTCString()+' '+route.name;
	}
	
	private getPage(page: number) {
		this.loading = true;
		const perPage:number = 8;
		const start = (page - 1) * perPage;
    	const end = start + perPage;
		
		this.asyncRoutesLabelList = this.routeService.getRoutesList()//actually this is not really server call
			.do(res => {
				this.total = res.length;
				this.page = page;
				this.loading = false;
			}).map(res => res.slice(start,end));
	}
}