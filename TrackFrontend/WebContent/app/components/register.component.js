"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var track_user_1 = require('../utilities/track-user');
var router_1 = require('@angular/router');
var authentication_service_1 = require('../services/authentication.service');
var RegisterComponent = (function () {
    //private submitted: boolean = false;
    function RegisterComponent(router, authenticationService) {
        this.router = router;
        this.authenticationService = authenticationService;
        this.loading = false;
        this.errorMessage = '';
        this.active = true;
        this.model = new track_user_1.TrackUser('', '', '', '', '', '', '');
    }
    RegisterComponent.prototype.doRegister = function () {
        var _this = this;
        console.log(this.diagnostic);
        this.loading = true;
        this.errorMessage = '';
        this.formReset();
        this.authenticationService.register(this.model)
            .subscribe(function (message) { return _this.onSuccessRegister(message); }, function (error) { return _this.onErrorRegister(error); }, function () { return _this.onCompleteRegister(); });
        //this.submitted=true;
    };
    RegisterComponent.prototype.formReset = function () {
        var _this = this;
        this.active = false;
        setTimeout(function () { return _this.active = true; }, 0);
    };
    RegisterComponent.prototype.onSuccessRegister = function (message) {
        this.errorMessage = message;
        console.log(this.diagnostic);
        this.loading = false;
    };
    RegisterComponent.prototype.onErrorRegister = function (error) {
        this.errorMessage = error;
        console.log(this.diagnostic);
        if (this.errorMessage == 'undefined -  undefined') {
            this.errorMessage = 'Cannot connect to server';
        }
        this.loading = false;
    };
    RegisterComponent.prototype.onCompleteRegister = function () {
        this.loading = false;
        this.router.navigate(['/home']);
    };
    Object.defineProperty(RegisterComponent.prototype, "diagnostic", {
        get: function () {
            return JSON.stringify(this.model)
                + '\n' + JSON.stringify(this.errorMessage);
        },
        enumerable: true,
        configurable: true
    });
    RegisterComponent = __decorate([
        core_1.Component({
            selector: 'register',
            templateUrl: 'app/components/register.component.html',
            styleUrls: ['app/components/register.component.css'],
        }), 
        __metadata('design:paramtypes', [router_1.Router, authentication_service_1.AuthenticationService])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map