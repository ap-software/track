"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/do');
require('rxjs/add/operator/delay');
var ng2_pagination_1 = require('ng2-pagination');
var track_user_service_1 = require('../services/track-user.service');
var FriendsListComponent = (function () {
    function FriendsListComponent(trackUserService) {
        this.trackUserService = trackUserService;
        this.sendTrackUserToParentEvent = new core_1.EventEmitter();
        this.page = 1;
        this.errorMessage = '';
    }
    FriendsListComponent.prototype.ngOnInit = function () {
        this.getPage(1);
    };
    FriendsListComponent.prototype.sendTrackUserToParent = function (user) {
        this.sendTrackUserToParentEvent.emit(user);
    };
    FriendsListComponent.prototype.getTrackUserLabel = function (trackUser) {
        return trackUser.username;
    };
    FriendsListComponent.prototype.getPage = function (page) {
        var _this = this;
        this.loading = true;
        var perPage = 8;
        var start = (page - 1) * perPage;
        var end = start + perPage;
        this.asyncTrackUsernameList = this.trackUserService.getIAmFriendList() //actually this is not really server call
            .do(function (res) {
            _this.total = res.length;
            _this.page = page;
            _this.loading = false;
        }).map(function (res) { return res.slice(start, end); });
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], FriendsListComponent.prototype, "sendTrackUserToParentEvent", void 0);
    FriendsListComponent = __decorate([
        core_1.Component({
            selector: 'friends-list',
            templateUrl: 'app/components/friends-list.component.html',
            styleUrls: ['app/components/friends-list.component.css'],
            directives: [ng2_pagination_1.PaginationControlsCmp],
            pipes: [ng2_pagination_1.PaginatePipe],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush,
            providers: [ng2_pagination_1.PaginationService]
        }), 
        __metadata('design:paramtypes', [track_user_service_1.TrackUserService])
    ], FriendsListComponent);
    return FriendsListComponent;
}());
exports.FriendsListComponent = FriendsListComponent;
//# sourceMappingURL=friends-list.component.js.map