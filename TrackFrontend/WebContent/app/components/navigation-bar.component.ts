import {ROUTER_DIRECTIVES} from '@angular/router';
import {Component} from '@angular/core';
import {TrackUser} from '../utilities/track-user';
import {Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {JsonWebToken} from '../utilities/json-web-token';


@Component({
	selector: 'navigation-bar',
	templateUrl: 'app/components/navigation-bar.component.html',
	//directives:[ROUTER_DIRECTIVES]
})

export class NavigationBarComponent{
	constructor(
		private router: Router,
		private authenticationService: AuthenticationService
	){}
	
	private doLogout(){
		this.authenticationService.logout();
	}

	public get diagnostic(){return JSON.stringify(this);}
}