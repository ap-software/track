"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var GoogleMapComponent = (function () {
    function GoogleMapComponent() {
        this.sendMapToParentEvent = new core_1.EventEmitter();
        this.track = new google.maps.Polyline();
        this.marker = new google.maps.Marker();
    }
    Object.defineProperty(GoogleMapComponent, "MIN_ZOOM", {
        get: function () { return 0; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GoogleMapComponent, "MAX_ZOOM", {
        get: function () { return 16; },
        enumerable: true,
        configurable: true
    });
    GoogleMapComponent.prototype.ngAfterViewInit = function () {
        var mapProp = {
            center: { lat: 0, lng: 0 },
            zoom: 2,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(document.getElementById(this.mapId), mapProp);
        //now we can transmit map to parent componenent
        this.sendMapToParentEvent.emit(this);
    };
    GoogleMapComponent.prototype.drawTrackOfRoute = function (trackLatLng, adjustMap) {
        this.trackLatLng = trackLatLng;
        this.track.setMap(null);
        this.marker.setMap(null);
        if (this.trackLatLng == null) {
            return;
        }
        if (adjustMap == true) {
            this.setCenterToTrack();
            this.setZoomToTrack();
        }
        this.track = new google.maps.Polyline({
            path: this.trackLatLng,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 3
        });
        this.track.setMap(this.map);
        this.marker = new google.maps.Marker({
            position: new google.maps.LatLng(this.trackLatLng[this.trackLatLng.length - 1].lat, this.trackLatLng[this.trackLatLng.length - 1].lng),
            map: this.map,
            title: 'user'
        });
    };
    GoogleMapComponent.prototype.clearMap = function () {
        this.track.setMap(null);
        this.marker.setMap(null);
    };
    GoogleMapComponent.prototype.getLatitudesArray = function () {
        return this.trackLatLng.map(function (footprint) { return footprint.lat; });
    };
    GoogleMapComponent.prototype.getLongitudesArray = function () {
        return this.trackLatLng.map(function (footprint) { return footprint.lng; });
    };
    GoogleMapComponent.prototype.getMinLatitude = function () {
        return Math.min.apply(Math, this.getLatitudesArray());
    };
    GoogleMapComponent.prototype.getMaxLatitude = function () {
        return Math.max.apply(Math, this.getLatitudesArray());
    };
    GoogleMapComponent.prototype.getMinLongitude = function () {
        return Math.min.apply(Math, this.getLongitudesArray());
    };
    GoogleMapComponent.prototype.getMaxLongitude = function () {
        return Math.max.apply(Math, this.getLongitudesArray());
    };
    GoogleMapComponent.prototype.setCenterToTrack = function () {
        var centerLongitude = (this.getMinLongitude() + this.getMaxLongitude()) / 2;
        var centerLatitude = (this.getMinLatitude() + this.getMaxLatitude()) / 2;
        this.map.setCenter({ lat: centerLatitude, lng: centerLongitude });
    };
    GoogleMapComponent.prototype.setZoomToTrack = function () {
        this.map.setZoom(GoogleMapComponent.MAX_ZOOM);
        var zoom = this.map.getZoom();
        while (true) {
            if (!this.isTrackInBounds()) {
                if (zoom <= GoogleMapComponent.MIN_ZOOM) {
                    break;
                }
                this.map.setZoom(--zoom);
            }
            else {
                this.map.setZoom(++zoom);
                if (!this.isTrackInBounds()) {
                    this.map.setZoom(--zoom);
                    break;
                }
                if (zoom >= GoogleMapComponent.MAX_ZOOM) {
                    break;
                }
            }
        }
    };
    GoogleMapComponent.prototype.isTrackInBounds = function () {
        var west = this.map.getBounds().getSouthWest().lng();
        var south = this.map.getBounds().getSouthWest().lat();
        var east = this.map.getBounds().getNorthEast().lng();
        var north = this.map.getBounds().getNorthEast().lat();
        if (west > this.getMinLongitude()) {
            return false;
        }
        if (east < this.getMaxLongitude()) {
            return false;
        }
        if (south > this.getMinLatitude()) {
            return false;
        }
        if (north < this.getMaxLatitude()) {
            return false;
        }
        return true;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], GoogleMapComponent.prototype, "mapId", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], GoogleMapComponent.prototype, "sendMapToParentEvent", void 0);
    GoogleMapComponent = __decorate([
        core_1.Component({
            selector: 'google-map',
            templateUrl: 'app/components/google-map.component.html',
            styleUrls: ['app/components/google-map.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], GoogleMapComponent);
    return GoogleMapComponent;
}());
exports.GoogleMapComponent = GoogleMapComponent;
//# sourceMappingURL=google-map.component.js.map