import {ChangeDetectionStrategy, Component, Input, Output, EventEmitter} from "@angular/core";

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import {Observable} from 'rxjs/Observable';
import {PaginatePipe, PaginationControlsCmp, PaginationService} from 'ng2-pagination';

import {TrackUserService} from '../services/track-user.service';
import {TrackUser} from '../utilities/track-user';

@Component({
	selector: 'friends-list',
	templateUrl: 'app/components/friends-list.component.html',
	styleUrls: ['app/components/friends-list.component.css'],
	directives: [PaginationControlsCmp],
	pipes: [PaginatePipe],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [PaginationService]
})	
export class FriendsListComponent{
	@Output() private sendTrackUserToParentEvent:EventEmitter<TrackUser> = new EventEmitter<TrackUser>();
	
	private asyncTrackUsernameList: Observable<TrackUser[]>;
	private total: number;
	
	private page:number=1;
	private errorMessage:string='';
	loading: boolean;
	
	constructor(
		private trackUserService:TrackUserService
	){}

	public ngOnInit() {
		this.getPage(1);
	}
	
	private sendTrackUserToParent(user:TrackUser){
		this.sendTrackUserToParentEvent.emit(user);
	}
	
	private getTrackUserLabel(trackUser:any):string{
		return trackUser.username;
	}
	
	private getPage(page: number) {
		this.loading = true;
		const perPage:number = 8;
		const start = (page - 1) * perPage;
    	const end = start + perPage;
		
		this.asyncTrackUsernameList = this.trackUserService.getIAmFriendList()//actually this is not really server call
			.do(res => {
				this.total = res.length;
				this.page = page;
				this.loading = false;
			}).map(res => res.slice(start,end));
	}
}