import {Component,AfterViewInit} from '@angular/core';
import {TrackUser} from '../utilities/track-user';
import {Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {TrackUserService} from '../services/track-user.service';
import {JsonWebToken} from '../utilities/json-web-token';

@Component({
	selector: 'personal-settings',
	templateUrl:'app/components/personal-settings.component.html',
	styleUrls: ['app/components/personal-settings.component.css'],
})

export class PersonalSettingsComponent implements AfterViewInit{
	private loadingChangePassword: boolean = false;
	private loadingChangePersonalData: boolean = false;
	private errorMessageChangePassword: string = '';
	private errorMessageChangePersonalData: string = '';
	
	private activeChangePassword: boolean = true;
	private activeChangePersonalData: boolean = true;
	//private submitted: boolean = false;
	model = new TrackUser('','','','','','','');
	constructor(
		private authenticationService: AuthenticationService,
		private trackUserService: TrackUserService
	){}
	
	public ngAfterViewInit(){
		//this.loadingChangePersonalData=true;
		this.getPersonalData();
	}
	
	private doChangePersonalData(){
		this.formChangePersonalDataReset();
		this.setPersonalData();
	}
	
	private doChangePassword(){
		this.formChangePasswordReset();
		this.setPassword();
	}
	
	public getPersonalData(){
		this.loadingChangePersonalData=true;
		this.trackUserService.getPersonalData()
				.subscribe(
						personalData=>this.onSuccessGetPersonalData(personalData),
						error => this.onErrorGetPersonalData(error),
						()=>this.onCompleteGetPersonalData()
						);
	}
	
	private onSuccessGetPersonalData(personalData:any){
		this.errorMessageChangePersonalData="";
		this.model.accessorUsername = personalData.username;
		//this.model.accessorPassword('');
		//this.model.accessorRepeatPassword('');
		this.model.accessorEmail = personalData.email;
		this.model.accessorPhone = personalData.phone;
		this.model.accessorName = personalData.name;
		this.model.accessorSurname = personalData.surname;
	}
	
	private onErrorGetPersonalData(error:any){
		this.errorMessageChangePersonalData=error;
		if(this.errorMessageChangePersonalData=='undefined -  undefined'){
			this.errorMessageChangePersonalData='Cannot connect to server';
		}
		this.loadingChangePersonalData=false;
	}
	
	private onCompleteGetPersonalData(){
		this.loadingChangePersonalData=false;		
	}
	
	public setPersonalData(){
		this.loadingChangePersonalData=true;
		this.trackUserService.setPersonalData(this.model)
				.subscribe(
						message=>this.onSuccessSetPersonalData(message),
						error => this.onErrorSetPersonalData(error),
						()=>this.onCompleteSetPersonalData()
						);
	}
	
	private onSuccessSetPersonalData(message:string){
		this.errorMessageChangePersonalData=message;
		this.loadingChangePersonalData=false;
	}
	
	private onErrorSetPersonalData(error:string){
		this.errorMessageChangePersonalData=error;
		console.log(this.diagnostic);
		if(this.errorMessageChangePersonalData=='undefined -  undefined'){
			this.errorMessageChangePersonalData='Cannot connect to server';
		}
		this.loadingChangePersonalData=false;
	}
	
	private onCompleteSetPersonalData(){
		this.loadingChangePersonalData=false;
		console.log(JSON.stringify(this.model));	
	}
	
	public setPassword(){
		this.loadingChangePassword = true;
		this.trackUserService.setPassword(this.model)
				.subscribe(
						message=>this.onSuccessSetPassword(message),
						error => this.onErrorSetPassword(error),
						()=>this.onCompleteSetPassword()
						);
	}
	
	private onSuccessSetPassword(message:string){
		this.errorMessageChangePassword = message;
		this.loadingChangePassword = false;
	}
	
	private onErrorSetPassword(error:string){
		this.errorMessageChangePassword=error;
		console.log(this.diagnostic);
		if(this.errorMessageChangePassword=='undefined -  undefined'){
			this.errorMessageChangePassword='Cannot connect to server';
		}
		this.loadingChangePassword=false;
	}
	
	private onCompleteSetPassword(){
		this.loadingChangePassword=false;
		console.log(JSON.stringify(this.model));	
	}
	
	private formChangePasswordReset(){
		this.activeChangePassword=false;
		setTimeout(()=>this.activeChangePassword=true,0);
	}
	
	private formChangePersonalDataReset(){
		this.activeChangePersonalData =false;
		setTimeout(()=>this.activeChangePersonalData=true,0);
	}
			
	public get diagnostic(){
		return JSON.stringify(this.model)
		//+'\n'+JSON.stringify(this.jwt)
		+'\n'+JSON.stringify(this.errorMessageChangePassword)
		+'\n'+JSON.stringify(this.errorMessageChangePersonalData);}
}