import {Component, Input, Output, AfterViewInit,  EventEmitter} from '@angular/core';


import {Footprint} from '../utilities/footprint';
import {Route} from '../utilities/route';
import {TRACKS} from '../mockes/track.mock';

@Component({
	selector: 'google-map',
	templateUrl: 'app/components/google-map.component.html',
	styleUrls: ['app/components/google-map.component.css']

})

export class GoogleMapComponent implements  AfterViewInit{
	@Input()	private mapId:string;
	@Output()	private sendMapToParentEvent:EventEmitter<GoogleMapComponent> = new EventEmitter<GoogleMapComponent>();
	
	public static get MIN_ZOOM() {return 0;}
	public static get MAX_ZOOM() {return 16;}
	
	private map:google.maps.Map;
	private track:google.maps.Polyline = new google.maps.Polyline();
	private marker:google.maps.Marker = new google.maps.Marker();  
  private trackLatLng;//:google.maps.LatLng[];
		
	public ngAfterViewInit(){
		var mapProp = {
			center: {lat: 0, lng: 0},  //this.trackLatLng[0],
			zoom: 2,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		this.map = new google.maps.Map(document.getElementById(this.mapId), mapProp);
		//now we can transmit map to parent componenent
		this.sendMapToParentEvent.emit(this);
	}
	
	public drawTrackOfRoute(trackLatLng:google.maps.LatLng[], adjustMap:boolean){
		this.trackLatLng=trackLatLng;
		this.track.setMap(null);
		this.marker.setMap(null);
		if(this.trackLatLng==null){return;}
    if(adjustMap==true){
      this.setCenterToTrack();
      this.setZoomToTrack();
    }
		this.track=new google.maps.Polyline({
			path: this.trackLatLng,
			geodesic: true,
			strokeColor: '#FF0000',
			strokeOpacity: 1.0,
			strokeWeight: 3
		});
        this.track.setMap(this.map);
		
		this.marker = new google.maps.Marker({
			position: new google.maps.LatLng(this.trackLatLng[this.trackLatLng.length-1].lat,
											this.trackLatLng[this.trackLatLng.length-1].lng),
          	map: this.map,
          	title: 'user'
		});
	}
  
  public clearMap(){
    this.track.setMap(null);
    this.marker.setMap(null);
  }	
	
	private getLatitudesArray():any[]{
		return  this.trackLatLng.map((footprint)=>{return footprint.lat;});
	}
	
	private getLongitudesArray():any[]{
		return  this.trackLatLng.map((footprint)=>{return footprint.lng;});
	}
	
	private getMinLatitude():any{
		return Math.min.apply(Math,this.getLatitudesArray());
	}
	
	private getMaxLatitude():any{
		return Math.max.apply(Math,this.getLatitudesArray());
	}
	
	private getMinLongitude():any{
		return Math.min.apply(Math,this.getLongitudesArray());
	}
	
	private getMaxLongitude():any{
		return Math.max.apply(Math,this.getLongitudesArray());
	}
	
	private setCenterToTrack(){
		let centerLongitude:number = (this.getMinLongitude()+this.getMaxLongitude())/2;
		let centerLatitude:number = (this.getMinLatitude()+this.getMaxLatitude())/2;
		this.map.setCenter({lat: centerLatitude, lng: centerLongitude});
		
	}
	
	private setZoomToTrack(){
    this.map.setZoom(GoogleMapComponent.MAX_ZOOM);
		let zoom:number = this.map.getZoom();
		while(true){
			if(!this.isTrackInBounds()){
				if(zoom<=GoogleMapComponent.MIN_ZOOM){break;}
				this.map.setZoom(--zoom);
			}
			else{
				this.map.setZoom(++zoom);
				if(!this.isTrackInBounds()){
					this.map.setZoom(--zoom);
					break;
				}
				if(zoom>=GoogleMapComponent.MAX_ZOOM){break;}
			}
		}
	}
	
	public isTrackInBounds():boolean{
		let west = this.map.getBounds().getSouthWest().lng();
		let south = this.map.getBounds().getSouthWest().lat();
		let east = this.map.getBounds().getNorthEast().lng();
		let north = this.map.getBounds().getNorthEast().lat();
		if(west > this.getMinLongitude()) {return false;}
		if(east < this.getMaxLongitude()) {return false;}
		if(south > this.getMinLatitude()){return false;}
		if(north < this.getMaxLatitude()){return false;}
		return true;				
	}
		
}