import {Component, OnDestroy} from '@angular/core';
import {NavigationBarComponent} from '../components/navigation-bar.component';
import {GoogleMapComponent} from '../components/google-map.component';
import {AuthenticationService} from '../services/authentication.service';
import {FootprintService} from '../services/footprint.service';
import {Constants} from '../utilities/constants';


@Component({
	selector: 'current-location',
	templateUrl: 'app/components/current-location.component.html',
	styleUrls : ['app/components/current-location.component.css'],
	//directives:[NavigationBarComponent,GoogleMapComponent]
})	
export class CurrentLocationComponent implements OnDestroy{
  
  private static get TIME_SHIFT():number{ return (-1)*20*1000;}
  
	private googleMapComponent:GoogleMapComponent;
  private timerToken: any;
  private errorMessage:string = '';
  private beginTime:Date;
  private trackLatLng;//:google.maps.LatLng[];
  private loading: boolean = false;
  private alreadyCentered:boolean=false;
  private dateLastKnownLocation:Date;
  
  constructor(
    private authenticationService:AuthenticationService,
    private footprintService:FootprintService
  ) {}
	
	//GoogleMapComponent send event after google map initialization
	private catchMapFromChild(googleMapComponent:GoogleMapComponent):void {
		this.googleMapComponent=googleMapComponent;
    let currentDate:Date=new Date();
    this.beginTime = new Date(currentDate.getTime()+CurrentLocationComponent.TIME_SHIFT);
		this.startCyclicServerPooling();
	}

	public ngOnDestroy(){
		this.stopCyclicServerPooling();
	}
  
  private startCyclicServerPooling(){
    this.drawCurrentTrackOfUser();
    this.timerToken=setInterval(()=>this.drawCurrentTrackOfUser(),Constants.poolingIntervalCurrentPosition);
  }
  
  private stopCyclicServerPooling(){
    clearTimeout(this.timerToken);
  }
	
	private onClickClearButton(){
		let currentDate:Date=new Date();
    this.beginTime = new Date(currentDate.getTime()+CurrentLocationComponent.TIME_SHIFT);
	}
  
  private drawCurrentTrackOfUser(){
    this.loading=true;
    this.errorMessage="Requesting for location update...";
    this.footprintService.getCurrentLocations(this.beginTime)
        .subscribe(
            points=>this.onSuccessGetCurrentLocations(points),
            error => this.onErrorGetCurrentLocations(error),
            ()=>this.onCompleteGetCurrentLocations()
            );
  }
  
  private onSuccessGetCurrentLocations(footprints:any){
    this.errorMessage="Data successfully received";
    this.trackLatLng = footprints.list.map((footprint)=>{return {lat: footprint.latitude, lng: footprint.longitude};});
    if(footprints.list!=null){
      if(footprints.list[0]!=null){
        this.dateLastKnownLocation=new Date(footprints.list[footprints.list.length-1].footprintCreationTime);
        return;
      }
    }
    this.dateLastKnownLocation = null;  
  }
  
  private onErrorGetCurrentLocations(error:any){
    this.loading=false;
    this.errorMessage=error;
    if(this.errorMessage=='undefined -  undefined'){
      this.errorMessage='Cannot connect to server';
    }
  }
  
  private onCompleteGetCurrentLocations(){
    if(this.trackLatLng!=null && this.trackLatLng.length>0){
      if((this.alreadyCentered==false) || (!this.googleMapComponent.isTrackInBounds())){
        this.googleMapComponent.drawTrackOfRoute(this.trackLatLng,true);
        this.alreadyCentered=true;
      }
      else{
        this.googleMapComponent.drawTrackOfRoute(this.trackLatLng,false);
      }
    }
    if(this.dateLastKnownLocation!=null){
      this.errorMessage="Last known location on "
        +Constants.daysOfWeek[this.dateLastKnownLocation.getDay()]+", "
        +this.dateLastKnownLocation.toLocaleDateString()+" "
        +this.dateLastKnownLocation.toLocaleTimeString();
    }
    else{
      this.errorMessage="There isn't any your location in database";
      this.googleMapComponent.clearMap();
    }
    this.loading=false;    
  }
}