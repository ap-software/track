"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
// Statics
require('rxjs/add/observable/of');
require('rxjs/add/observable/throw');
// Operators
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
require('rxjs/add/operator/debounceTime');
require('rxjs/add/operator/distinctUntilChanged');
require('rxjs/add/operator/switchMap');
require('rxjs/add/operator/toPromise');
var Observable_1 = require('rxjs/Observable');
var http_1 = require('@angular/http');
var constants_1 = require('../utilities/constants');
var AuthenticationService = (function () {
    //public isLoggedIn: boolean = false;
    function AuthenticationService(http) {
        this.http = http;
        this.authenticationUrl = constants_1.Constants.serverUrl + '/rest/AuthenticationService'; // URL to web API
    }
    Object.defineProperty(AuthenticationService.prototype, "accessorJsonWebToken", {
        get: function () {
            return this.jsonWebToken = JSON.parse(sessionStorage.getItem('JsonWebToken'));
        },
        set: function (jwt) {
            console.log("zapis do sesji:" + JSON.stringify(jwt));
            sessionStorage.setItem('JsonWebtoken', JSON.stringify(jwt));
        },
        enumerable: true,
        configurable: true
    });
    /*
    public getJsonWebToken():JsonWebToken{
        return <JsonWebToken> JSON.parse(sessionStorage.getItem('JsonWebToken'));
    }
    
    public setJsonWebToken(jwt:JsonWebToken):void{
        console.log("zapis do sesji:"+JSON.stringify(jwt));
        sessionStorage.setItem('JsonWebToken', JSON.stringify(jwt));
    }
    */
    AuthenticationService.prototype.login = function (username, password) {
        var headers = new http_1.Headers({ "username": username, "password": password });
        return this.http.get(this.authenticationUrl + '/login', { headers: headers })
            .map(this.extractDataLogin)
            .catch(this.handleErrorLogin);
    };
    AuthenticationService.prototype.extractDataLogin = function (res) {
        console.log("Response" + JSON.stringify(res));
        var body = res.json();
        sessionStorage.setItem('JsonWebToken', JSON.stringify(body));
        return body;
    };
    AuthenticationService.prototype.extractDataRegister = function (res) {
        console.log("Response" + JSON.stringify(res));
        var message = res.json().text;
        return message;
    };
    AuthenticationService.prototype.handleErrorLogin = function (error) {
        console.log(" AuthenticationService.handleError");
        var body = error.json();
        var errMsg = body.statusCode + " -  " + body.text;
        sessionStorage.setItem('JsonWebToken', JSON.stringify(null));
        return Observable_1.Observable.throw(errMsg);
    };
    AuthenticationService.prototype.handleErrorRegister = function (error) {
        console.log(" AuthenticationService.handleError");
        var body = error.json();
        var errMsg = body.statusCode + " -  " + body.text;
        return Observable_1.Observable.throw(errMsg);
    };
    AuthenticationService.prototype.register = function (user) {
        var body = JSON.stringify(user);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(this.authenticationUrl + '/register', body, options)
            .map(this.extractDataRegister)
            .catch(this.handleErrorRegister);
    };
    /*
    login(username:string, password:string){
        for(let item of USERS){
            console.log("item.username:"+item.accessorUsername+" |username:"+username);
            console.log("item.password:"+item.accessorPassword+" |password:"+password);
            if((item.accessorUsername==username) && (item.accessorPassword==password)){
                //this.isLoggedIn=true;
                this.isLoggedIn=true;
                console.log("Logged in !!!"+"this.isLoggedIn:"+this.isLoggedIn);
                this.jsonWebToken.accessorUsername=item.accessorUsername;
                break;
            }
        }
    }
    */
    AuthenticationService.prototype.logout = function () {
        sessionStorage.setItem('JsonWebToken', JSON.stringify(null));
    };
    AuthenticationService.prototype.check = function () {
        //	console.log("token sesji:"+JSON.stringify(this.jsonWet
        if (this.accessorJsonWebToken) {
            return Observable_1.Observable.of(true);
        }
        else {
            return Observable_1.Observable.of(false);
        }
    };
    AuthenticationService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], AuthenticationService);
    return AuthenticationService;
}());
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map