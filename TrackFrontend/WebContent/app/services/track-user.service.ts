import {Injectable} from '@angular/core';

import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
// Operators
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';

import {Observable} from 'rxjs/Observable';
import {AuthenticationService} from '../services/authentication.service';
import {TRACKS} from '../mockes/track.mock'
import {Http, Response, Headers,RequestOptions} from '@angular/http';
import {JsonWebToken} from '../utilities/json-web-token';
import {Constants} from '../utilities/constants';
import {TrackUser} from '../utilities/track-user';

/* Service to get data from server about user's device location.*/

@Injectable()
export class TrackUserService{
	private trackUserServiceUrl = Constants.serverUrl+'/rest/UserService';  // URL to web API
	
	constructor(
		private authenticationService:AuthenticationService,
		private http: Http
	) {}

  public getIAmFriendList():Observable<any[]>{
    let headers = new Headers({"jsonWebToken":JSON.stringify(this.authenticationService.accessorJsonWebToken)});
    return this.http.get(this.trackUserServiceUrl+'/list/iamfriend',{headers:headers})
      .map(this.extractDataGetIAmFriendList)
      .catch(this.handleError);
  }
  
  private extractDataGetIAmFriendList(res: Response) {
    let friends = res.json();
    return friends.list;
  }
  
	public getFriendsList():Observable<any[]>{
		let headers = new Headers({"jsonWebToken":JSON.stringify(this.authenticationService.accessorJsonWebToken)});
		return this.http.get(this.trackUserServiceUrl+'/list/friends',{headers:headers})
			.map(this.extractDataGetFriendsList)
			.catch(this.handleError);
	}
	
	private extractDataGetFriendsList(res: Response) {
		let friends = res.json();
		return friends.list;
	}
	
	public getPersonalData():Observable<any[]>{
		let headers = new Headers({"jsonWebToken":JSON.stringify(this.authenticationService.accessorJsonWebToken)});
		return this.http.get(this.trackUserServiceUrl+'/details',{headers:headers})
			.map(this.extractDataGetPersonalData)
			.catch(this.handleError);
	}

	private extractDataGetPersonalData(res: Response) {
		let user = res.json();
		return user;
	}
		
	public setPersonalData(user:TrackUser):Observable<string> {
		let body = JSON.stringify(user);
		let headers = new Headers({"jsonWebToken":JSON.stringify(this.authenticationService.accessorJsonWebToken), 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		
    	return this.http.post(this.trackUserServiceUrl+'/change/personal-data',body,options)
			.map(this.extractDataSetPersonalData)
			.catch(this.handleError);
	}

	private extractDataSetPersonalData(res: Response) {
		console.log("Response"+JSON.stringify(res));
		let message:string = res.json().text;
		return message;
	}
	
	public addOwnFriend(friendUsername:string):Observable<string> {
		let body = JSON.stringify(friendUsername);
		let headers = new Headers({ "jsonWebToken":JSON.stringify(this.authenticationService.accessorJsonWebToken),
									"friendUsername":friendUsername,
									'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		
    	return this.http.post(this.trackUserServiceUrl+'/add/friend',body,options)
			.map(this.extractDataAddOwnFriend)
			.catch(this.handleError);
	}

	private extractDataAddOwnFriend(res: Response) {
		console.log("Response"+JSON.stringify(res));
		let message:string = res.json().text;
		return message;
	}
	
	public removeOwnFriend(friend:TrackUser):Observable<string> {
		let body = JSON.stringify(friend);
		let headers = new Headers({ "jsonWebToken":JSON.stringify(this.authenticationService.accessorJsonWebToken),
									'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		
    	return this.http.post(this.trackUserServiceUrl+'/remove/friend',body,options)
			.map(this.extractDataRemoveOwnFriend)
			.catch(this.handleError);
	}

	private extractDataRemoveOwnFriend(res: Response) {
		console.log("Response"+JSON.stringify(res));
		let message:string = res.json().text;
		return message;
	}
	
	public setPassword(user:TrackUser):Observable<string> {
		let body = JSON.stringify(user);
		let headers = new Headers({"jsonWebToken":JSON.stringify(this.authenticationService.accessorJsonWebToken), 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		
    	return this.http.post(this.trackUserServiceUrl+'/change/password',body,options)
			.map(this.extractDataSetPassword)
			.catch(this.handleError);
	}

	private extractDataSetPassword(res: Response) {
		console.log("Response"+JSON.stringify(res));
		let message:string = res.json().text;
		return message;
	}
		
	private handleError (error: Response) {
		let body = error.json();
		let errMsg = `${body.statusCode} -  ${body.text}`;
		return Observable.throw(errMsg);
	}
	
}
