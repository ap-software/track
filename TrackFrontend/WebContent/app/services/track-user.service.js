"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
require('rxjs/add/observable/of');
require('rxjs/add/observable/throw');
// Operators
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
require('rxjs/add/operator/debounceTime');
require('rxjs/add/operator/distinctUntilChanged');
require('rxjs/add/operator/switchMap');
require('rxjs/add/operator/toPromise');
var Observable_1 = require('rxjs/Observable');
var authentication_service_1 = require('../services/authentication.service');
var http_1 = require('@angular/http');
var constants_1 = require('../utilities/constants');
/* Service to get data from server about user's device location.*/
var TrackUserService = (function () {
    function TrackUserService(authenticationService, http) {
        this.authenticationService = authenticationService;
        this.http = http;
        this.trackUserServiceUrl = constants_1.Constants.serverUrl + '/rest/UserService'; // URL to web API
    }
    TrackUserService.prototype.getIAmFriendList = function () {
        var headers = new http_1.Headers({ "jsonWebToken": JSON.stringify(this.authenticationService.accessorJsonWebToken) });
        return this.http.get(this.trackUserServiceUrl + '/list/iamfriend', { headers: headers })
            .map(this.extractDataGetIAmFriendList)
            .catch(this.handleError);
    };
    TrackUserService.prototype.extractDataGetIAmFriendList = function (res) {
        var friends = res.json();
        return friends.list;
    };
    TrackUserService.prototype.getFriendsList = function () {
        var headers = new http_1.Headers({ "jsonWebToken": JSON.stringify(this.authenticationService.accessorJsonWebToken) });
        return this.http.get(this.trackUserServiceUrl + '/list/friends', { headers: headers })
            .map(this.extractDataGetFriendsList)
            .catch(this.handleError);
    };
    TrackUserService.prototype.extractDataGetFriendsList = function (res) {
        var friends = res.json();
        return friends.list;
    };
    TrackUserService.prototype.getPersonalData = function () {
        var headers = new http_1.Headers({ "jsonWebToken": JSON.stringify(this.authenticationService.accessorJsonWebToken) });
        return this.http.get(this.trackUserServiceUrl + '/details', { headers: headers })
            .map(this.extractDataGetPersonalData)
            .catch(this.handleError);
    };
    TrackUserService.prototype.extractDataGetPersonalData = function (res) {
        var user = res.json();
        return user;
    };
    TrackUserService.prototype.setPersonalData = function (user) {
        var body = JSON.stringify(user);
        var headers = new http_1.Headers({ "jsonWebToken": JSON.stringify(this.authenticationService.accessorJsonWebToken), 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(this.trackUserServiceUrl + '/change/personal-data', body, options)
            .map(this.extractDataSetPersonalData)
            .catch(this.handleError);
    };
    TrackUserService.prototype.extractDataSetPersonalData = function (res) {
        console.log("Response" + JSON.stringify(res));
        var message = res.json().text;
        return message;
    };
    TrackUserService.prototype.addOwnFriend = function (friendUsername) {
        var body = JSON.stringify(friendUsername);
        var headers = new http_1.Headers({ "jsonWebToken": JSON.stringify(this.authenticationService.accessorJsonWebToken),
            "friendUsername": friendUsername,
            'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(this.trackUserServiceUrl + '/add/friend', body, options)
            .map(this.extractDataAddOwnFriend)
            .catch(this.handleError);
    };
    TrackUserService.prototype.extractDataAddOwnFriend = function (res) {
        console.log("Response" + JSON.stringify(res));
        var message = res.json().text;
        return message;
    };
    TrackUserService.prototype.removeOwnFriend = function (friend) {
        var body = JSON.stringify(friend);
        var headers = new http_1.Headers({ "jsonWebToken": JSON.stringify(this.authenticationService.accessorJsonWebToken),
            'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(this.trackUserServiceUrl + '/remove/friend', body, options)
            .map(this.extractDataRemoveOwnFriend)
            .catch(this.handleError);
    };
    TrackUserService.prototype.extractDataRemoveOwnFriend = function (res) {
        console.log("Response" + JSON.stringify(res));
        var message = res.json().text;
        return message;
    };
    TrackUserService.prototype.setPassword = function (user) {
        var body = JSON.stringify(user);
        var headers = new http_1.Headers({ "jsonWebToken": JSON.stringify(this.authenticationService.accessorJsonWebToken), 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(this.trackUserServiceUrl + '/change/password', body, options)
            .map(this.extractDataSetPassword)
            .catch(this.handleError);
    };
    TrackUserService.prototype.extractDataSetPassword = function (res) {
        console.log("Response" + JSON.stringify(res));
        var message = res.json().text;
        return message;
    };
    TrackUserService.prototype.handleError = function (error) {
        var body = error.json();
        var errMsg = body.statusCode + " -  " + body.text;
        return Observable_1.Observable.throw(errMsg);
    };
    TrackUserService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [authentication_service_1.AuthenticationService, http_1.Http])
    ], TrackUserService);
    return TrackUserService;
}());
exports.TrackUserService = TrackUserService;
//# sourceMappingURL=track-user.service.js.map