import {Injectable} from '@angular/core';

import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
// Operators
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';

import {Observable} from 'rxjs/Observable';
import {AuthenticationService} from '../services/authentication.service';
import {TRACKS} from '../mockes/track.mock'
import {Http, Response, Headers,RequestOptions} from '@angular/http';
import {JsonWebToken} from '../utilities/json-web-token';
import {Constants} from '../utilities/constants';
import {Footprint} from '../utilities/footprint';
import {Route} from '../utilities/route';
import {TrackUser} from '../utilities/track-user';


/* Service to get data from server about user's device location.*/

@Injectable()
export class FootprintService{
	private footprintServiceUrl = Constants.serverUrl+'/rest/FootprintService';  // URL to web API
	
	constructor(
		private authenticationService:AuthenticationService,
		private http: Http
	) {}

	public getCurrentLocations(timestamp:Date):Observable<Footprint[]>{
		console.log("getCurrentLocations:"+JSON.stringify(this.authenticationService.accessorJsonWebToken));
		let headers = new Headers({"timestamp":timestamp.toISOString(),"jsonWebToken":JSON.stringify(this.authenticationService.accessorJsonWebToken)});
		console.log(JSON.stringify(headers));
		return this.http.get(this.footprintServiceUrl+'/last',{headers:headers})
			.map(this.extractData)
			.catch(this.handleError);
	}

	private extractData(res: Response) {
		console.log("Response"+JSON.stringify(res));
		let footprints:Footprint[] = res.json();
		return footprints;
	}
		
	private handleError (error: Response) {
		let body = error.json();
		let errMsg = `${body.statusCode} -  ${body.text}`;
		//sessionStorage.setItem('JsonWebToken', JSON.stringify(null));
		return Observable.throw(errMsg);
	}
	
	public getRouteFootprints(route:Route){
		let headers = new Headers({'jsonWebToken':JSON.stringify(this.authenticationService.accessorJsonWebToken)
									,'route':JSON.stringify(route)});
		let options = new RequestOptions({ headers: headers });
		
		console.log(JSON.stringify(headers));
		return this.http.get(this.footprintServiceUrl+'/list/route',options)
			.map(this.extractData)
			.catch(this.handleError);
	}
	
	public getFriendCurrentFootprints(friend:TrackUser, date:Date){
		console.log("getFriendFootprints:"+JSON.stringify(this.authenticationService.accessorJsonWebToken));
		let headers = new Headers({'jsonWebToken':JSON.stringify(this.authenticationService.accessorJsonWebToken)
									,'friend':JSON.stringify(friend)
									,'timestamp':date.toISOString()});
		let options = new RequestOptions({ headers: headers });
		
		console.log(JSON.stringify(headers));
		return this.http.get(this.footprintServiceUrl+'/last/friend',options)
			.map(this.extractData)
			.catch(this.handleError);
	}
}
