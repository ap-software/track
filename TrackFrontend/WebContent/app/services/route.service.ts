import {Injectable} from '@angular/core';

import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
// Operators
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';

import {Observable} from 'rxjs/Observable';
import {AuthenticationService} from '../services/authentication.service';
import {TRACKS} from '../mockes/track.mock'
import {Http, Response, Headers,RequestOptions} from '@angular/http';
import {JsonWebToken} from '../utilities/json-web-token';
import {Constants} from '../utilities/constants';
import {Route} from '../utilities/route';

/* Service to get data from server about user's device location.*/

@Injectable()
export class RouteService{
	private routeServiceUrl = Constants.serverUrl+'/rest/RouteService';  // URL to web API
	
	constructor(
		private authenticationService:AuthenticationService,
		private http: Http
	) {}

	/*
	getCurrentLocations(beginTime:Date):google.maps.LatLng[]{
		let last:google.maps.LatLng=TRACKS[TRACKS.length-1];
		let latitude:number=last.lat()+(Math.random()-0.5)/32;
		let longitude:number=last.lng()+(Math.random()-0.5)/32;

		let newFootprint:google.maps.LatLng= new google.maps.LatLng(latitude,longitude);
		TRACKS.push(newFootprint);
		
		return TRACKS;
	}
	*/

	getRoutesList():Observable<any[]>{
		let headers = new Headers({"jsonWebToken":JSON.stringify(this.authenticationService.accessorJsonWebToken)});
		return this.http.get(this.routeServiceUrl+'/list',{headers:headers})
			.map(this.extractData)
			.catch(this.handleError);
	}

		private extractData(res: Response) {
		//console.log("Response"+JSON.stringify(res));
		let routes = res.json();
		return routes.list;
	}
		
	private handleError (error: Response) {
		let body = error.json();
		let errMsg = `${body.statusCode} -  ${body.text}`;
		//sessionStorage.setItem('JsonWebToken', JSON.stringify(null));
		return Observable.throw(errMsg);
	}
	
}
