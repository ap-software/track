import {Injectable} from '@angular/core';
// Statics
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
// Operators
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';

import {Observable} from 'rxjs/Observable';
import {JsonWebToken} from '../utilities/json-web-token';
import {USERS} from '../mockes/mock-users';
import {Http, Response, Headers,RequestOptions} from '@angular/http';
import {Constants} from '../utilities/constants';
import {TrackUser} from '../utilities/track-user';


@Injectable()
export class AuthenticationService{
	
	private authenticationUrl = Constants.serverUrl+'/rest/AuthenticationService';  // URL to web API
	private jsonWebToken: JsonWebToken;
	
	//public isLoggedIn: boolean = false;
	constructor(private http: Http){}
	
	public get accessorJsonWebToken():JsonWebToken{
		return this.jsonWebToken=<JsonWebToken> JSON.parse(sessionStorage.getItem('JsonWebToken'));
	}
	
	public set accessorJsonWebToken(jwt:JsonWebToken){
		console.log("zapis do sesji:"+JSON.stringify(jwt));
		sessionStorage.setItem('JsonWebtoken', JSON.stringify(jwt));
	}
	/*
	public getJsonWebToken():JsonWebToken{
		return <JsonWebToken> JSON.parse(sessionStorage.getItem('JsonWebToken'));
	}
	
	public setJsonWebToken(jwt:JsonWebToken):void{
		console.log("zapis do sesji:"+JSON.stringify(jwt));
		sessionStorage.setItem('JsonWebToken', JSON.stringify(jwt));
	}
	*/
	
	public login(username:string, password:string):Observable<JsonWebToken> {
		let headers = new Headers({"username":username,"password":password});	
    	return this.http.get(this.authenticationUrl+'/login',{headers:headers})
			.map(this.extractDataLogin)
			.catch(this.handleErrorLogin);
	}
	
	private extractDataLogin(res: Response) {
		console.log("Response"+JSON.stringify(res));
		let body:JsonWebToken = res.json();
		sessionStorage.setItem('JsonWebToken', JSON.stringify(body));
		return body;
	}
	
	private extractDataRegister(res: Response) {
		console.log("Response"+JSON.stringify(res));
		let message:string = res.json().text;
		return message;
	}
	
	private handleErrorLogin (error: Response) {
		console.log(" AuthenticationService.handleError");
		let body = error.json();
		let errMsg = `${body.statusCode} -  ${body.text}`;
		sessionStorage.setItem('JsonWebToken', JSON.stringify(null));
		return Observable.throw(errMsg);
	}
	
	private handleErrorRegister (error: Response) {
		console.log(" AuthenticationService.handleError");
		let body = error.json();
		let errMsg = `${body.statusCode} -  ${body.text}`;
		return Observable.throw(errMsg);
	}
	
	public register(user:TrackUser):Observable<string> {
		let body = JSON.stringify(user);
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		
    	return this.http.post(this.authenticationUrl+'/register',body,options)
			.map(this.extractDataRegister)
			.catch(this.handleErrorRegister);
	}
	
	/*
	login(username:string, password:string){
		for(let item of USERS){
			console.log("item.username:"+item.accessorUsername+" |username:"+username);
			console.log("item.password:"+item.accessorPassword+" |password:"+password);
			if((item.accessorUsername==username) && (item.accessorPassword==password)){
				//this.isLoggedIn=true;
				this.isLoggedIn=true;
				console.log("Logged in !!!"+"this.isLoggedIn:"+this.isLoggedIn);
				this.jsonWebToken.accessorUsername=item.accessorUsername;
				break;
			}
		}
	}
	*/
	
	logout(){
		sessionStorage.setItem('JsonWebToken', JSON.stringify(null));		
	}
	
	check(){
	//	console.log("token sesji:"+JSON.stringify(this.jsonWet
		if(this.accessorJsonWebToken){
			return Observable.of(true);
		}
		else{
			return Observable.of(false);
		}
	}
	

}
