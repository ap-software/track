"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
require('rxjs/add/observable/of');
require('rxjs/add/observable/throw');
// Operators
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
require('rxjs/add/operator/debounceTime');
require('rxjs/add/operator/distinctUntilChanged');
require('rxjs/add/operator/switchMap');
require('rxjs/add/operator/toPromise');
var Observable_1 = require('rxjs/Observable');
var authentication_service_1 = require('../services/authentication.service');
var http_1 = require('@angular/http');
var constants_1 = require('../utilities/constants');
/* Service to get data from server about user's device location.*/
var RouteService = (function () {
    function RouteService(authenticationService, http) {
        this.authenticationService = authenticationService;
        this.http = http;
        this.routeServiceUrl = constants_1.Constants.serverUrl + '/TrackBackend/rest/EventService'; // URL to web API
    }
    /*
    getCurrentLocations(beginTime:Date):google.maps.LatLng[]{
        let last:google.maps.LatLng=TRACKS[TRACKS.length-1];
        let latitude:number=last.lat()+(Math.random()-0.5)/32;
        let longitude:number=last.lng()+(Math.random()-0.5)/32;

        let newFootprint:google.maps.LatLng= new google.maps.LatLng(latitude,longitude);
        TRACKS.push(newFootprint);
        
        return TRACKS;
    }
    */
    RouteService.prototype.getRoutesList = function () {
        var headers = new http_1.Headers({ "jsonWebToken": JSON.stringify(this.authenticationService.accessorJsonWebToken) });
        return this.http.get(this.routeServiceUrl + '/list', { headers: headers })
            .map(this.extractData)
            .catch(this.handleError);
    };
    RouteService.prototype.extractData = function (res) {
        //console.log("Response"+JSON.stringify(res));
        var routes = res.json();
        return routes;
    };
    RouteService.prototype.handleError = function (error) {
        var body = error.json();
        var errMsg = body.statusCode + " -  " + body.text;
        //sessionStorage.setItem('JsonWebToken', JSON.stringify(null));
        return Observable_1.Observable.throw(errMsg);
    };
    RouteService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [authentication_service_1.AuthenticationService, http_1.Http])
    ], RouteService);
    return RouteService;
}());
exports.RouteService = RouteService;
//# sourceMappingURL=track-event.service.js.map