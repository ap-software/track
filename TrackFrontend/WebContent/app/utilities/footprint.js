"use strict";
var Footprint = (function () {
    function Footprint(routeId, latitude, longitude, accuracy, footprintCreationTime) {
        this.routeId = routeId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;
        this.footprintCreationTime = footprintCreationTime;
    }
    Object.defineProperty(Footprint.prototype, "accessorRouteId", {
        get: function () { return this.routeId; },
        set: function (routeId) { this.routeId = routeId; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Footprint.prototype, "accessorLatitude", {
        get: function () { return this.latitude; },
        set: function (latitude) { this.latitude = latitude; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Footprint.prototype, "accessorLongitude", {
        get: function () { return this.longitude; },
        set: function (longitude) { this.longitude = longitude; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Footprint.prototype, "accessorFootprintCreationTime", {
        get: function () { return this.footprintCreationTime; },
        set: function (footprintCreationTime) { this.footprintCreationTime = footprintCreationTime; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Footprint.prototype, "accessorAccuracy", {
        get: function () { return this.accuracy; },
        set: function (accuracy) { this.accuracy = accuracy; },
        enumerable: true,
        configurable: true
    });
    return Footprint;
}());
exports.Footprint = Footprint;
//# sourceMappingURL=footprint.js.map