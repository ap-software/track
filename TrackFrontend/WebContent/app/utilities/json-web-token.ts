export class JsonWebToken{
		
	constructor(
		public token:string,
		public username:string
		){}
	
	public get accessorToken():string{return this.token;}
	public set accessorToken(token:string){this.token=token;}
	
	
	
	public get accessorUsername():string{return this.username;}
	public set accessorUsername(username:string){this.username=username;}
	//public getUsername():string{return this.username;}
	
	
}