export class Footprint{
	constructor(
		private routeId: number,
		private latitude:number,
		private longitude:number,
		private accuracy:number,
		private footprintCreationTime: Date
		){}
		
	public get accessorRouteId():number{return this.routeId;}
	public get accessorLatitude():number{return this.latitude;}
	public get accessorLongitude():number{return this.longitude;}
	public get accessorFootprintCreationTime():Date{return this.footprintCreationTime;}
	public get accessorAccuracy():number{return this.accuracy;}
	
	public set accessorRouteId(routeId: number){this.routeId= routeId;}
	public set accessorLatitude(latitude:number){this.latitude=latitude;}
	public set accessorLongitude(longitude:number){this.longitude=longitude}
	public set accessorFootprintCreationTime(footprintCreationTime: Date){this.footprintCreationTime=footprintCreationTime;}
	public set accessorAccuracy(accuracy: number){this.accuracy=accuracy;}

	
}