"use strict";
var JsonWebToken = (function () {
    function JsonWebToken(token, username) {
        this.token = token;
        this.username = username;
    }
    Object.defineProperty(JsonWebToken.prototype, "accessorToken", {
        get: function () { return this.token; },
        set: function (token) { this.token = token; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(JsonWebToken.prototype, "accessorUsername", {
        get: function () { return this.username; },
        set: function (username) { this.username = username; },
        enumerable: true,
        configurable: true
    });
    return JsonWebToken;
}());
exports.JsonWebToken = JsonWebToken;
//# sourceMappingURL=json-web-token.js.map