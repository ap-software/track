"use strict";
var Event = (function () {
    function Event(id, name, eventCreationTime) {
        this.id = id;
        this.name = name;
        this.eventCreationTime = eventCreationTime;
        this.toLabel = function () { return 'ADAM'; };
    }
    Object.defineProperty(Event.prototype, "accessorId", {
        get: function () { return this.id; },
        set: function (id) { this.id = id; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Event.prototype, "accessorName", {
        get: function () { return this.name; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Event.prototype, "accessorEventCreationTime", {
        get: function () { return this.eventCreationTime; },
        set: function (eventCreationTime) { this.eventCreationTime = eventCreationTime; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Event.prototype, "accessorLatitude", {
        set: function (name) { this.name = name; },
        enumerable: true,
        configurable: true
    });
    return Event;
}());
exports.Event = Event;
//# sourceMappingURL=event.js.map