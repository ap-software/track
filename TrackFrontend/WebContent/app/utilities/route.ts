export class Route{
	constructor(
		private id: number,
		private name:string,
		private routeCreationTime: Date
		){}
		
	public get accessorId():number{return this.id;}
	public get accessorName():string{return this.name;}
	public get accessorRouteCreationTime():Date{return this.routeCreationTime;}
	
	public set accessorId(id: number){this.id= id;}
	public set accessorLatitude(name:string){this.name=name;}
	public set accessorRouteCreationTime(routeCreationTime: Date){this.routeCreationTime=routeCreationTime;}
	
	public toLabel=():string=>{return 'ADAM';}
	
}