"use strict";
var TrackUser = (function () {
    function TrackUser(username, password, repeatPassword, email, name, surname, phone) {
        this.username = username;
        this.password = password;
        this.repeatPassword = repeatPassword;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
    }
    Object.defineProperty(TrackUser.prototype, "accessorUsername", {
        get: function () { return this.username; },
        set: function (username) { this.username = username; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TrackUser.prototype, "accessorPassword", {
        get: function () { return this.password; },
        set: function (password) { this.password = password; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TrackUser.prototype, "accessorRepeatPassword", {
        get: function () { return this.repeatPassword; },
        set: function (repeatPassword) { this.repeatPassword = repeatPassword; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TrackUser.prototype, "accessorEmail", {
        get: function () { return this.email; },
        set: function (email) { this.email = email; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TrackUser.prototype, "accessorName", {
        get: function () { return this.name; },
        set: function (name) { this.name = name; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TrackUser.prototype, "accessorSurname", {
        get: function () { return this.surname; },
        set: function (surname) { this.surname = surname; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TrackUser.prototype, "accessorPhone", {
        get: function () { return this.phone; },
        set: function (phone) { this.phone = phone; },
        enumerable: true,
        configurable: true
    });
    return TrackUser;
}());
exports.TrackUser = TrackUser;
//# sourceMappingURL=track-user.js.map