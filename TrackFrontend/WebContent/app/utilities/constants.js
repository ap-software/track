"use strict";
var Constants = (function () {
    function Constants() {
    }
    Constants.serverUrl = 'http://localhost:8080/TrackBackend'; //'https://track-backend.herokuapp.com';//
    Constants.poolingIntervalCurrentPosition = 10000; //[ms]
    Constants.daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return Constants;
}());
exports.Constants = Constants;
//# sourceMappingURL=constants.js.map