export class TrackUser{
	constructor(
		private username:string,
		private password: string,
		private repeatPassword: string,
		private email: string,
		private name: string,
		private surname: string,
		private phone: string
		){}
		
	public get accessorUsername():string{return this.username;}
	public get accessorPassword():string{return this.password;}
	public get accessorRepeatPassword():string{return this.repeatPassword;}
	public get accessorEmail():string{return this.email;}
	public get accessorName():string{return this.name;}
	public get accessorSurname():string{return this.surname;}
	public get accessorPhone():string{return this.phone;}
	
	public set accessorUsername(username:string){this.username=username;}
	public set accessorPassword(password: string){this.password=password;}
	public set accessorRepeatPassword(repeatPassword: string){this.repeatPassword=repeatPassword;}
	public set accessorEmail(email:string){this.email=email;}
	public set accessorName(name: string){this.name=name;}
	public set accessorSurname(surname:string){this.surname=surname;}
	public set accessorPhone(phone: string){this.phone=phone;}
		
}