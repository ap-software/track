"use strict";
var Route = (function () {
    function Route(id, name, routeCreationTime) {
        this.id = id;
        this.name = name;
        this.routeCreationTime = routeCreationTime;
        this.toLabel = function () { return 'ADAM'; };
    }
    Object.defineProperty(Route.prototype, "accessorId", {
        get: function () { return this.id; },
        set: function (id) { this.id = id; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Route.prototype, "accessorName", {
        get: function () { return this.name; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Route.prototype, "accessorRouteCreationTime", {
        get: function () { return this.routeCreationTime; },
        set: function (routeCreationTime) { this.routeCreationTime = routeCreationTime; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Route.prototype, "accessorLatitude", {
        set: function (name) { this.name = name; },
        enumerable: true,
        configurable: true
    });
    return Route;
}());
exports.Route = Route;
//# sourceMappingURL=route.js.map