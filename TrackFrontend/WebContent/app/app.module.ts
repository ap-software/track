import {NgModule}       from '@angular/core';
import {BrowserModule}  from '@angular/platform-browser';
import {FormsModule}    from '@angular/forms';
import {HttpModule}     from '@angular/http';

import {AppComponent} from './components/app.component';
import {routing, appRoutingProviders} from './routes/app.routing';

import {LoginComponent} from './components/login.component';
import {RegisterComponent} from './components/register.component'; 
import {HomeComponent} from './components/home.component';
import {CurrentLocationComponent} from './components/current-location.component';
import {PreviousTracksComponent} from './components/previous-tracks.component';
import {FriendsLocationComponent} from './components/friends-location.component';
import {NavigationBarComponent} from './components/navigation-bar.component';
import {GoogleMapComponent} from './components/google-map.component';
import {RoutesListComponent} from './components/routes-list.component';
import {FriendsListComponent} from './components/friends-list.component';
import {OwnFriendsListComponent} from './components/own-friends-list.component';
import {PersonalSettingsComponent} from './components/personal-settings.component';
import {AuthenticationService} from './services/authentication.service';
import {FootprintService} from './services/footprint.service';
import {RouteService} from './services/route.service';
import {TrackUserService} from './services/track-user.service';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    routing,
	HttpModule
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NavigationBarComponent,
    HomeComponent,
    CurrentLocationComponent,
    PreviousTracksComponent,
    GoogleMapComponent,
  RoutesListComponent,
  FriendsLocationComponent,
	FriendsListComponent,
	OwnFriendsListComponent,
	PersonalSettingsComponent,
  ],
  providers: [
    appRoutingProviders,
	AuthenticationService,
	FootprintService,
	RouteService,
	TrackUserService
  ],
  bootstrap: [ AppComponent]
})
export class AppModule {}