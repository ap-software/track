import {ROUTER_DIRECTIVES} from '@angular/router';
import {Component} from '@angular/core';
import {MapComponent} from './map.component';


@Component({
	selector: 'my-app',
	templateUrl: 'app/components/app.component.html',
	directives: [MapComponent]
})

export class AppComponent{}