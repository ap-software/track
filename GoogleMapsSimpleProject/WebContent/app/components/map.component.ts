import {Component, OnInit} from '@angular/core';


@Component({
	selector: 'map',
	templateUrl: 'app/components/map.component.html'

})

export class MapComponent implements OnInit{
	
	constructor(){}
	ngOnInit() {
		var mapProp = {
			center: new google.maps.LatLng(51.508742, -0.120850),
			zoom: 5,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
	}
}