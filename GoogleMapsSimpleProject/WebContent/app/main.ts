import {bootstrap} from '@angular/platform-browser-dynamic';
import {AppComponent} from './components/app.component';


var appPromise=bootstrap(AppComponent).catch(err => console.error(err));