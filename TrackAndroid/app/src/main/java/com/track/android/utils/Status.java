package com.track.android.utils;

/**
 * Created by APOKLADE on 04/01/2017.
 */

public enum Status {

    SUCCESS("Success"),
    ERROR("Unrecognized error"),
    CONNECTION_FAILED("Connection failed"),

    AUTHENTICATION_FAILED("Wrong username and/or password"),
    AUTHENTICATION_SUCCESS("Authentication success"),

    FOOTPRINT_ADDED_SUCCESSFULLY("Footprint added successfully"),

    ROUTES_LIST_RECEIVED_SUCCESSFULLY("List of routes successfully received"),
    ROUTE_FOUND_SUCCESSFULLY("Route found successfully"),
    ROUTE_ADDED_SUCCESSFULLY("Route added successfully"),
    ROUTE_REMOVED_SUCCESSFULLY("Route removed successfully");

    private String message;

    Status(String msg) {
        message=msg;
    }

    public String getMessage(){
        return message;
    }
    public void setMessage(String msg){message=msg;}

}
