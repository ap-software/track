package com.track.android.services;

import android.os.SystemClock;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.track.android.entities.Route;
import com.track.android.services.rest.AuthenticationClient;
import com.track.android.services.rest.RouteClient;
import com.track.android.utils.Constants;
import com.track.android.utils.JsonWebToken;
import com.track.android.utils.JsonableList;
import com.track.android.utils.ListenerAsyncTask;
import com.track.android.utils.Status;
import com.track.android.utils.TextFeedback;

import java.io.IOException;
import java.net.ConnectException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteService {

	private static final String TAG = "RouteService";
	private Status completionStatus;

	private Route currentRoute;

	private List<Route> routesList;

	private static AuthenticationService authenticationService;


	private RouteService(){}
	
	private static RouteService instance;
	
	public static RouteService getInstance(){
		if (instance == null){
			instance = new RouteService();
		}
		if(authenticationService==null){
			authenticationService = AuthenticationService.getInstance();
		}
		return instance;
	}

	public void requestListOfRoutes(OnTaskCompleted listener){
		ListenerAsyncTask doGetListOfRoutes = new ListenerAsyncTask(listener){

			Boolean isRequestCompleted = false;

			@Override
			protected Object doInBackground(Object... params) {

				try{
					routesList=null;
					RouteClient client = RouteClient.retrofit.create(RouteClient.class);
					Call<JsonableList<Route>> call = client.requestListOfRoutes(authenticationService.getJsonWebToken().toJSON());
					call.enqueue(new Callback<JsonableList<Route>>() {

						@Override
						public void onResponse(Call<JsonableList<Route>> call, Response<JsonableList<Route>> response) {
							if (response.isSuccessful()){
								completionStatus = com.track.android.utils.Status.ROUTES_LIST_RECEIVED_SUCCESSFULLY;
								routesList = response.body().getList();
							}else{
								try {
									Gson gson = new GsonBuilder().create();
									TextFeedback responseError = gson.fromJson(response.errorBody().string(),TextFeedback.class);
									completionStatus = com.track.android.utils.Status.ERROR;
									completionStatus.setMessage(responseError.getText());
								} catch (IOException e) {
									completionStatus = com.track.android.utils.Status.ERROR;
									e.printStackTrace();
								}
							}

							isRequestCompleted=true;
						}

						@Override
						public void onFailure(Call<JsonableList<Route>> call, Throwable t) {
							try{
								throw t;
							}catch (ConnectException e){
								completionStatus = com.track.android.utils.Status.CONNECTION_FAILED;
							}catch (Throwable e){
								completionStatus = com.track.android.utils.Status.ERROR;
							}

							isRequestCompleted=true;
						}
					});
				}catch (Exception e){
					isRequestCompleted=true;
					completionStatus = com.track.android.utils.Status.ERROR;
				}

				while (isRequestCompleted==false){
					SystemClock.sleep(Constants.AWAITING_INTERVAL);
				}
				return completionStatus;
			}
		};
		doGetListOfRoutes.execute();
	}

	public void requestRouteName(OnTaskCompleted listener, final String routeName){
		ListenerAsyncTask doGetRouteIdByRouteName = new ListenerAsyncTask(listener){

			Boolean isRequestCompleted = false;

			@Override
			protected Object doInBackground(Object... params) {

				try{
					currentRoute=null;
					RouteClient client = RouteClient.retrofit.create(RouteClient.class);
					Call<Route> call = client.requestRouteByName(authenticationService.getJsonWebToken().toJSON(),routeName);
					call.enqueue(new Callback<Route>() {

						@Override
						public void onResponse(Call<Route> call, Response<Route> response) {
							if (response.isSuccessful()){
								completionStatus = com.track.android.utils.Status.ROUTE_FOUND_SUCCESSFULLY;
								currentRoute = response.body();
							}else{
								try {
									Gson gson = new GsonBuilder().create();
									TextFeedback responseError = gson.fromJson(response.errorBody().string(),TextFeedback.class);
									completionStatus = com.track.android.utils.Status.ERROR;
									completionStatus.setMessage(responseError.getText());
								} catch (IOException e) {
									completionStatus = com.track.android.utils.Status.ERROR;
									e.printStackTrace();
								}
							}

							isRequestCompleted=true;
						}

						@Override
						public void onFailure(Call<Route> call, Throwable t) {
							try{
								throw t;
							}catch (ConnectException e){
								completionStatus = com.track.android.utils.Status.CONNECTION_FAILED;
							}catch (Throwable e){
								completionStatus = com.track.android.utils.Status.ERROR;
							}

							isRequestCompleted=true;
						}
					});
				}catch (Exception e){
					isRequestCompleted=true;
					completionStatus = com.track.android.utils.Status.ERROR;
				}

				while (isRequestCompleted==false){
					SystemClock.sleep(Constants.AWAITING_INTERVAL);
				}
				return completionStatus;
			}
		};
		doGetRouteIdByRouteName.execute();
	}

	public void createRoute(OnTaskCompleted listener, final Route route){
		ListenerAsyncTask doCreateRoute = new ListenerAsyncTask(listener){

			Boolean isRequestCompleted = false;

			@Override
			protected Object doInBackground(Object... params) {

				try{
					RouteClient client = RouteClient.retrofit.create(RouteClient.class);
					Call<TextFeedback> call = client.add(authenticationService.getJsonWebToken().toJSON(),route);
					call.enqueue(new Callback<TextFeedback>() {

						@Override
						public void onResponse(Call<TextFeedback> call, Response<TextFeedback> response) {
							if (response.isSuccessful()){
								completionStatus = com.track.android.utils.Status.ROUTE_ADDED_SUCCESSFULLY;
								TextFeedback responseError = response.body();
							}else{
								try {
									Gson gson = new GsonBuilder().create();
									TextFeedback responseError = gson.fromJson(response.errorBody().string(),TextFeedback.class);
									completionStatus = com.track.android.utils.Status.ERROR;
									completionStatus.setMessage(responseError.getText());
								} catch (IOException e) {
									completionStatus = com.track.android.utils.Status.ERROR;
									e.printStackTrace();
								}
							}

							isRequestCompleted=true;
						}

						@Override
						public void onFailure(Call<TextFeedback> call, Throwable t) {
							try{
								throw t;
							}catch (ConnectException e){
								completionStatus = com.track.android.utils.Status.CONNECTION_FAILED;
							}catch (Throwable e){
								completionStatus = com.track.android.utils.Status.ERROR;
							}

							isRequestCompleted=true;
						}
					});
				}catch (Exception e){
					isRequestCompleted=true;
					completionStatus = com.track.android.utils.Status.ERROR;
				}

				while (isRequestCompleted==false){
					SystemClock.sleep(Constants.AWAITING_INTERVAL);
				}
				return completionStatus;
			}
		};
		doCreateRoute.execute();
	}

	public void removeRoute(OnTaskCompleted listener, final Route route){
		ListenerAsyncTask doRemoveRoute = new ListenerAsyncTask(listener){

			Boolean isRequestCompleted = false;

			@Override
			protected Object doInBackground(Object... params) {

				try{
					RouteClient client = RouteClient.retrofit.create(RouteClient.class);
					Call<TextFeedback> call = client.remove(authenticationService.getJsonWebToken().toJSON(),route);
					call.enqueue(new Callback<TextFeedback>() {

						@Override
						public void onResponse(Call<TextFeedback> call, Response<TextFeedback> response) {
							if (response.isSuccessful()){
								completionStatus = com.track.android.utils.Status.ROUTE_REMOVED_SUCCESSFULLY;
								TextFeedback responseError = response.body();
							}else{
								try {
									Gson gson = new GsonBuilder().create();
									TextFeedback responseError = gson.fromJson(response.errorBody().string(),TextFeedback.class);
									completionStatus = com.track.android.utils.Status.ERROR;
									completionStatus.setMessage(responseError.getText());
								} catch (IOException e) {
									completionStatus = com.track.android.utils.Status.ERROR;
									e.printStackTrace();
								}
							}

							isRequestCompleted=true;
						}

						@Override
						public void onFailure(Call<TextFeedback> call, Throwable t) {
							try{
								throw t;
							}catch (ConnectException e){
								completionStatus = com.track.android.utils.Status.CONNECTION_FAILED;
							}catch (Throwable e){
								completionStatus = com.track.android.utils.Status.ERROR;
							}

							isRequestCompleted=true;
						}
					});
				}catch (Exception e){
					isRequestCompleted=true;
					completionStatus = com.track.android.utils.Status.ERROR;
				}

				while (isRequestCompleted==false){
					SystemClock.sleep(Constants.AWAITING_INTERVAL);
				}
				return completionStatus;
			}
		};
		doRemoveRoute.execute();
	}

	public Route getCurrentRoute() {
		return currentRoute;
	}

	public List<Route> getRoutesList() {
		return routesList;
	}

	public void setRoutesList(List<Route> routesList) {
		this.routesList = routesList;
	}
}
