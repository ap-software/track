package com.track.android.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.test.suitebuilder.TestMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.track.android.R;
import com.track.android.entities.Route;
import com.track.android.services.OnTaskCompleted;
import com.track.android.services.RouteService;
import com.track.android.utils.Status;
import com.track.android.utils.ViewIdGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class RouteHistory extends AppCompatActivity implements OnTaskCompleted{

    private List<Route> routes;
    private Map<Integer,Route> viewIdToRouteMap = new HashMap<Integer,Route>();
    private Integer candidateToRemove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_history);
        findViewById(R.id.progressBarRouteHistory).setVisibility(View.VISIBLE);

        Thread t =new Thread(new Runnable() {
            @Override
            public void run() {
                requestListOfRoutes();
            }
        });
        t.start();
    }

    private void openRouteTrackingActivity(Route route){
        Intent intent = new Intent(this, RouteTracking.class);
        intent.putExtra(RouteTracking.ROUTE_TO_TRACK, Route.toJson(route));
        startActivity(intent);
        finish();
    }

    protected void doBack(View view){
        finish();
    }

    private void addButtonRoute(LinearLayout layout, Route route){
        Button routeButton = new Button(this);
        routeButton.setText(route.getName());
        routeButton.setId(ViewIdGenerator.generate(this));
        viewIdToRouteMap.put(routeButton.getId(),route);
        routeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRouteTrackingActivity(viewIdToRouteMap.get(v.getId()));
            }
        });
        layout.addView(routeButton,new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                0.25f));
    }

    private void addButtonDelete(LinearLayout layout, Route route){
        Button deleteButton = new Button(this);
        deleteButton.setText("X");
        deleteButton.setId(ViewIdGenerator.generate(this));
        viewIdToRouteMap.put(deleteButton.getId(),route);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Route route = viewIdToRouteMap.get(v.getId());
                candidateToRemove = v.getId();
                deleteRoute(route);
                /*LinearLayout ll = (LinearLayout) v.getParent();
                ll.removeAllViews();*/

            }
        });
        layout.addView(deleteButton,new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                0.75f));
    }

    private void addRouteItem(Route route){
        LinearLayout listRoutesLayout = (LinearLayout) findViewById(R.id.linearLayoutRouteHistoryList);

        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.HORIZONTAL);
        ll.setId(route.getId());

        listRoutesLayout.addView(ll,new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        addButtonRoute(ll,route);
        addButtonDelete(ll,route);
    }

    private void createRoutesButtonsList(){
        for(final Route route: routes){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    addRouteItem(route);
                }
            });
        }
    }

    private void requestListOfRoutes(){
        RouteService routeService = RouteService.getInstance();
        routeService.requestListOfRoutes(this);
    }

    private void deleteRoute(final Route route){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        findViewById(R.id.progressBarRouteHistory).setVisibility(View.VISIBLE);
                        TextView errorMessage = (TextView) findViewById(R.id.textViewRouteHistoryErrorMessage);
                        errorMessage.setText("Removing route "+ route.getName()+"...");
                        RouteService routeService = RouteService.getInstance();
                        routeService.removeRoute(RouteHistory.this,route);
                    }
                });

            }
        });
        t.start();
    }

    private void mockListOfRoutes(){
        RouteService routeService = RouteService.getInstance();

        routes = new ArrayList<>();
        for(int i=0; i<10;i++){
            Route r = Route.createRoute("bieg0"+i);
            r.setId(i);
            routes.add(r);
            SystemClock.sleep(250);
        }
        routeService.setRoutesList(routes);
        onTaskCompleted(Status.ROUTES_LIST_RECEIVED_SUCCESSFULLY);
    }

    @Override
    public void onTaskCompleted(final Status completionStatus) {
        final RouteService routeService=RouteService.getInstance();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView errorMessage = (TextView) findViewById(R.id.textViewRouteHistoryErrorMessage);
                errorMessage.setText(completionStatus.getMessage());
                findViewById(R.id.progressBarRouteHistory).setVisibility(View.INVISIBLE);
            }
        });
        switch (completionStatus){
            case ROUTES_LIST_RECEIVED_SUCCESSFULLY:
                Thread th = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        routes = routeService.getRoutesList();
                        createRoutesButtonsList();
                    }
                });
                th.start();
                break;
            case ROUTE_REMOVED_SUCCESSFULLY:
                TextView errorMessage = (TextView) findViewById(R.id.textViewRouteHistoryErrorMessage);
                errorMessage.setText(completionStatus.getMessage());
                LinearLayout ll  = (LinearLayout) findViewById(candidateToRemove).getParent();
                ll.removeAllViews();
                break;
            case CONNECTION_FAILED:
                break;
            case ERROR:
                break;
        }
    }
}
