package com.track.android.entities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

/**
 * Created by APOKLADE on 09/01/2017.
 */

public class Route {

    private int id;
    private String name;
    private java.util.Date routeCreationTime;

    private Route() {
        super();
    }

    public static Route createRoute(String routeName){
        Route route = new Route();
        route.setName(routeName);
        route.setRouteCreationTime(new Date());
        return route;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public java.util.Date getRouteCreationTime() {
        return routeCreationTime;
    }

    public void setRouteCreationTime(java.util.Date routeCreationTime) {
        this.routeCreationTime = routeCreationTime;
    }

    public static String toJson( Route route) {
        Gson gson = new Gson();
        return gson.toJson(route);
    }

    public static Route fromJson(String jsonRoute){
        Gson gson = new Gson();
        Route route = new Route();
        return route = gson.fromJson(jsonRoute, Route.class);
    }
}