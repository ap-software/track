package com.track.android.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

public class JsonWebToken {
	
	private String token;
	private String username;

	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String toJSON(){
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}
}
