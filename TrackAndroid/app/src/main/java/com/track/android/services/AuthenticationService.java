package com.track.android.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.track.android.services.rest.AuthenticationClient;
import com.track.android.utils.Constants;
import com.track.android.utils.JsonWebToken;
import com.track.android.utils.Status;
import com.track.android.utils.TextFeedback;
import com.track.android.utils.ListenerAsyncTask;

import android.os.SystemClock;

import java.io.IOException;
import java.net.ConnectException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthenticationService {

	private static final String TAG = "AuthenticationService";

	private JsonWebToken jsonWebToken;
	private Status completionStatus;

	private AuthenticationService(){}
	
	private static AuthenticationService instance;
	
	public static AuthenticationService getInstance(){
		if (instance == null){
			instance = new AuthenticationService();
		}
		return instance;
	}

	public JsonWebToken getJsonWebToken() {
		return jsonWebToken;
	}

	public void setJsonWebToken(JsonWebToken jsonWebToken) {
		this.jsonWebToken = jsonWebToken;
	}
	
	public void authenticate(OnTaskCompleted listener, final String username, final String password){
		ListenerAsyncTask doAuthenticate = new ListenerAsyncTask(listener){

			Boolean isRequestCompleted = false;

			@Override
			protected Object doInBackground(Object... params) {

				try{
                    AuthenticationClient client = AuthenticationClient.retrofit.create(AuthenticationClient.class);
                    Call<JsonWebToken> call = client.login(username, password);
                    call.enqueue(new Callback<JsonWebToken>() {

						@Override
                        public void onResponse(Call<JsonWebToken> call, Response<JsonWebToken> response) {
							if (response.isSuccessful()){
								completionStatus = com.track.android.utils.Status.AUTHENTICATION_SUCCESS;
								jsonWebToken=response.body();
							}else{
								try {
									Gson gson = new GsonBuilder().create();
									TextFeedback responseError = gson.fromJson(response.errorBody().string(),TextFeedback.class);
									completionStatus = com.track.android.utils.Status.AUTHENTICATION_FAILED;

									//errorMessage = responseError.getStatusCode() + ", " + responseError.getText();
								} catch (IOException e) {
									completionStatus = com.track.android.utils.Status.ERROR;
									e.printStackTrace();
								}
							}

							isRequestCompleted=true;
						}

                        @Override
                        public void onFailure(Call<JsonWebToken> call, Throwable t) {
							try{
								throw t;
							}catch (ConnectException e){
								completionStatus = com.track.android.utils.Status.CONNECTION_FAILED;
							}catch (Throwable e){
								completionStatus = com.track.android.utils.Status.ERROR;
							}

							isRequestCompleted=true;
						}
                    });
				}catch (Exception e){
					isRequestCompleted=true;
					completionStatus = com.track.android.utils.Status.ERROR;
				}

				while (isRequestCompleted==false){
					SystemClock.sleep(Constants.AWAITING_INTERVAL);
				}
                return completionStatus;
			}
		 };
		 doAuthenticate.execute();
	}

}
