package com.track.android.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.track.android.R;
import com.track.android.entities.Route;
import com.track.android.services.OnTaskCompleted;
import com.track.android.services.RouteService;
import com.track.android.utils.Status;


public class CreatingRoute extends AppCompatActivity implements OnTaskCompleted {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creating_route);
    }

    protected void doCancel(View view) {
        finish();
    }

    protected void doCreate(View view) {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarCreateRoute);
        progressBar.setVisibility(View.VISIBLE);
        EditText routeNameEditText = (EditText) findViewById(R.id.routeName);
        TextView errorMessage= (TextView) findViewById(R.id.textViewErrorCreateRoute);
        String routeName = routeNameEditText.getText().toString();

        RouteService routeService = RouteService.getInstance();

        if (routeName==null || routeName.equals("")){
            progressBar.setVisibility(View.INVISIBLE);
            errorMessage.setText("Route name cannot be empty");
            return;
        }
        routeService.createRoute(this,Route.createRoute(routeName));
    }

    private void openRouteTrackingActivity(Route currentRoute){
        Intent intent = new Intent(this, RouteTracking.class);
        intent.putExtra(RouteTracking.ROUTE_TO_TRACK, Route.toJson(currentRoute));
        startActivity(intent);
        finish();
    }

    private void doGetRouteByRouteName(){
        EditText routeNameEditText = (EditText) findViewById(R.id.routeName);
        String routeName = routeNameEditText.getText().toString();
        RouteService routeService = RouteService.getInstance();
        routeService.requestRouteName(this,routeName);
    }

    @Override
    public void onTaskCompleted(Status completionStatus) {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarCreateRoute);
        progressBar.setVisibility(View.INVISIBLE);

        TextView errorMessageEditText= (TextView) findViewById(R.id.textViewErrorCreateRoute);
        errorMessageEditText.setText(completionStatus.getMessage());

        RouteService routeService = RouteService.getInstance();

        switch (completionStatus){
            case CONNECTION_FAILED:
                //openBeginTrackingActivity();
                break;
            case ERROR:
                //openBeginTrackingActivity();
                break;
            case ROUTE_ADDED_SUCCESSFULLY:
                progressBar.setVisibility(View.VISIBLE);
                doGetRouteByRouteName();
                break;
            case ROUTE_FOUND_SUCCESSFULLY:
                openRouteTrackingActivity(routeService.getCurrentRoute());
            default:
                break;
        }
    }
}
