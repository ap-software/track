package com.track.android.services.rest;

import android.widget.TextView;

import com.track.android.utils.Constants;
import com.track.android.utils.JsonWebToken;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by APOKLADE on 11/12/2016.
 */

public interface AuthenticationClient {

    @GET("login")
    Call<JsonWebToken> login(@Header("username") String username, @Header("password") String password);

    public static final String authenticationServiceUrl =
            Constants.SERVER_URL + "rest/AuthenticationService/";

    public static final OkHttpClient httpClient = new OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .build();

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(authenticationServiceUrl)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
