package com.track.android.utils;

import java.util.List;

/**
 * Created by APOKLADE on 10/02/2017.
 */

public class JsonableList <T> {

    private List<T> list;
/*  private JsonableList(){};

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static JsonableList fromList(List list){
        JsonableList jsonableList = new JsonableList();
        jsonableList.list=list;
        return jsonableList;
    }*/
    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}