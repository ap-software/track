package com.track.android.utils;

/**
 * Created by APOKLADE on 05/01/2017.
 */

public class TextFeedback {

    private int statusCode;
    private String text;

    public static TextFeedback create(int statusCode, String text){
        TextFeedback feedback = new TextFeedback();
        feedback.setText(text);
        feedback.setStatusCode(statusCode);
        return feedback;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
