package com.track.android.services.rest;

import com.google.gson.GsonBuilder;
import com.track.android.entities.Footprint;
import com.track.android.entities.Route;
import com.track.android.utils.Constants;
import com.track.android.utils.TextFeedback;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by APOKLADE on 11/12/2016.
 */

public interface FootprintClient {

    @POST("add")
    Call<TextFeedback> add(@Header("jsonWebToken") String jsonWebToken, @Body Footprint footprint);

    public static final String footprintServiceUrl =
            Constants.SERVER_URL + "rest/FootprintService/";

    public static final OkHttpClient httpClient = new OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .build();

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(footprintServiceUrl)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(
                    new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create()))
            .build();

}
