package com.track.android.services.rest;

import com.google.gson.GsonBuilder;
import com.track.android.entities.Route;
import com.track.android.utils.Constants;
import com.track.android.utils.JsonWebToken;
import com.track.android.utils.JsonableList;
import com.track.android.utils.TextFeedback;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by APOKLADE on 11/12/2016.
 */

public interface RouteClient {

    @POST("add")
    Call<TextFeedback> add(@Header("jsonWebToken") String jsonWebToken, @Body Route route);

    @POST("remove")
    Call<TextFeedback> remove(@Header("jsonWebToken") String jsonWebToken, @Body Route route);

    @GET("get/route")
    Call<Route> requestRouteByName(@Header("jsonWebToken") String jsonWebToken, @Header("routeName") String routeName);

    @GET("list")
    Call <JsonableList<Route>> requestListOfRoutes(@Header("jsonWebToken") String jsonWebToken);


    public static final String routeServiceUrl =
            Constants.SERVER_URL + "rest/RouteService/";

    public static final OkHttpClient httpClient = new OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .build();

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(routeServiceUrl)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(
                    new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create()))
            .build();

}
