package com.track.android.services;

import com.track.android.utils.Status;

/**
 * Created by APOKLADE on 04/01/2017.
 */

public interface OnTaskCompleted {
    public void onTaskCompleted(final Status completionStatus);
}
