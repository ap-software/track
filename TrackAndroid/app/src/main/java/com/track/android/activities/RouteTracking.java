package com.track.android.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.track.android.R;
import com.track.android.entities.Footprint;
import com.track.android.entities.Route;
import com.track.android.services.FootprintService;
import com.track.android.services.LocationService;
import com.track.android.services.OnTaskCompleted;
import com.track.android.utils.Status;

import java.math.BigDecimal;

public class RouteTracking extends AppCompatActivity implements OnTaskCompleted {

    private static final String TAG = RouteTracking.class.getName();
    public final static String ROUTE_TO_TRACK = "com.track.android.activities.ROUTE_TO_TRACK";

    private final static int REFRESH_LOCATION_INTERVAL = 10000; //[ms]
    private final static int LOCATION_SCALE = 5;//[ms]
    private final static long TWO_MINUTE = 120000000000l; //[ns]

    private Location location;

    private boolean isTrackingEnabled=false;
    private Route route;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        route = Route.fromJson(intent.getStringExtra(ROUTE_TO_TRACK));
        setContentView(R.layout.activity_route_tracking);
    }

    protected void doBack(View v){
        finish();
        Intent intent = new Intent(this, BeginTracking.class);
        startActivity(intent);
    }

    protected void doStart(View view){
        findViewById(R.id.progressBarRouteTrackingGps).setVisibility(View.VISIBLE);
        findViewById(R.id.progressBarRouteTrackingNetwork).setVisibility(View.VISIBLE);
        findViewById(R.id.progressBarRouteTrackingServer).setVisibility(View.VISIBLE);

        LocationService locationService = LocationService.getInstance(this);
        locationService.start();
        isTrackingEnabled=true;
        Thread t =new Thread(new Runnable() {
            @Override
            public void run() {
                while(isTrackingEnabled){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            RouteTracking.this.requestLocation();
                        }
                    });
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            RouteTracking.this.sendLocationToServer();
                        }
                    });
                    SystemClock.sleep(REFRESH_LOCATION_INTERVAL);
                }
            }
        });
        t.start();
    }

    protected void doStop(View view){
        findViewById(R.id.progressBarRouteTrackingGps).setVisibility(View.INVISIBLE);
        findViewById(R.id.progressBarRouteTrackingNetwork).setVisibility(View.INVISIBLE);
        findViewById(R.id.progressBarRouteTrackingServer).setVisibility(View.INVISIBLE);

        TextView textViewGpsMessage = (TextView) findViewById(R.id.textViewGpsMessage);
        textViewGpsMessage.setText("GPS: disabled");
        TextView textViewNetworkMessage = (TextView) findViewById(R.id.textViewNetworkMessage);
        textViewNetworkMessage.setText("NETWORK: disabled");
        TextView textViewServerMessage = (TextView) findViewById(R.id.textViewServerMessage);
        textViewServerMessage.setText("SERVER: disconnected");

        LocationService locationService = LocationService.getInstance(this);
        locationService.stop();
        isTrackingEnabled=false;
    }

    private void requestLocation() {
        LocationService locationService = LocationService.getInstance(this);
        Location locationGps = locationService.getLocationGps();
        Location locationNetwork = locationService.getLocationNetwork();
        if(locationGps!=null){
            Log.v(TAG,"SystemClock.elapsedRealtimeNanos():"+SystemClock.elapsedRealtimeNanos()
                    +" locationGps.getElapsedRealtimeNanos():"+locationGps.getElapsedRealtimeNanos()
                    +" delta:"+(SystemClock.elapsedRealtimeNanos()-locationGps.getElapsedRealtimeNanos())
                    );
            if((SystemClock.elapsedRealtimeNanos()-locationGps.getElapsedRealtimeNanos())>TWO_MINUTE){
                Log.v(TAG,"zerowanie locationGps");
                locationGps=null;
            }
        }
        if(locationNetwork!=null){
            Log.v(TAG,"SystemClock.elapsedRealtimeNanos():"+SystemClock.elapsedRealtimeNanos()
                    +" locationNetwork.getElapsedRealtimeNanos():"+locationNetwork.getElapsedRealtimeNanos()
                    +" delta:"+(SystemClock.elapsedRealtimeNanos()-locationNetwork.getElapsedRealtimeNanos())
            );
            if((SystemClock.elapsedRealtimeNanos()-locationNetwork.getElapsedRealtimeNanos())>TWO_MINUTE){
                Log.v(TAG,"zerowanie locationNetwork");
                locationNetwork=null;
            }
        }
        if ((locationGps != null) && (locationNetwork != null)) {
            if (locationGps.getAccuracy() < locationGps.getAccuracy()) {
                location = locationGps;
            } else {
                location = locationNetwork;
            }
        } else {
            if (locationGps != null) {
                location = locationGps;
            } else if (locationNetwork != null) {
                location = locationNetwork;
            } else{
                location = null;
            }
        }
        updateLocationTextViews();
    }

    private void sendLocationToServer(){
        FootprintService footprintService = FootprintService.getInstance();
        TextView textGps = (TextView) findViewById(R.id.textViewServerMessage);
        if(location==null){
            textGps.setText("Waiting for location signal...");
            return;
        }
        textGps.setText("Transferring to server...");
        footprintService.addFootprint(this,Footprint.createFootprint(route.getId(),location));

    }

    private void updateLocationTextViews(){
        LocationService locationService = LocationService.getInstance(this);
        Location locationGps = locationService.getLocationGps();
        Location locationNetwork = locationService.getLocationNetwork();
        TextView textGps = (TextView) findViewById(R.id.textViewGpsMessage);
        TextView textNetwork = (TextView) findViewById(R.id.textViewNetworkMessage);
        if(locationGps!=null){
            textGps.setText("GPS: lat:"
                    + String.format("%."+LOCATION_SCALE+"f",locationGps.getLatitude())
                    + " lng:"
                    + String.format("%."+LOCATION_SCALE+"f",locationGps.getLongitude()));
        }else{
            textGps.setText("GPS: Waiting for signal...");
        }
        if(locationNetwork!=null){
            textNetwork.setText("NETWORK: lat:"
                    + String.format("%."+LOCATION_SCALE+"f",locationNetwork.getLatitude())
                    + " lng:"
                    + String.format("%."+LOCATION_SCALE+"f",locationNetwork.getLongitude()));
        }else{
            textNetwork.setText("NETWORK: Waiting for signal...");
        }
    }

    @Override
    public void onTaskCompleted(Status completionStatus) {
        FootprintService footprintService = FootprintService.getInstance();
        TextView textGps = (TextView) findViewById(R.id.textViewServerMessage);
        switch (completionStatus){
            case CONNECTION_FAILED:
                textGps.setText(completionStatus.getMessage());
                break;
            case ERROR:
                textGps.setText(completionStatus.getMessage());
                break;
            case FOOTPRINT_ADDED_SUCCESSFULLY:
                textGps.setText(completionStatus.getMessage());
                break;
            default:
                break;
        }
    }
}
