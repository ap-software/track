package com.track.android.utils;

import android.os.AsyncTask;

import com.track.android.services.OnTaskCompleted;

/**
 * Created by APOKLADE on 04/01/2017.
 */

public abstract class ListenerAsyncTask extends AsyncTask<Object,Object,Object> { //change Object to required type
    private OnTaskCompleted listener;

    public ListenerAsyncTask(OnTaskCompleted listener){
        this.listener=listener;
    }

    @Override
    protected void onPostExecute(Object o){
        listener.onTaskCompleted((com.track.android.utils.Status) o);
    }
}