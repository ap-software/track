package com.track.android.entities;

import android.location.Location;

import com.google.gson.Gson;

import java.util.Date;

/**
 * Created by APOKLADE on 09/01/2017.
 */

public class Footprint {

    private int id;
    private double latitude;
    private double longitude;
    private double accuracy;

    private java.util.Date footprintCreationTime;
    int routeId;

    public int getId() {return id;}
    public void setId(int id) {this.id = id;}

    public double getLatitude() {return latitude;}
    public void setLatitude(double latitude) {this.latitude = latitude;}

    public double getLongitude() {return longitude;}
    public void setLongitude(double longitude) {this.longitude = longitude;}

    public java.util.Date getFootprintCreationTime() {return footprintCreationTime;}
    public void setFootprintCreationTime(java.util.Date footprintCreationTime) {this.footprintCreationTime = footprintCreationTime;}

    public int getRouteId() {return routeId;}
    public void setRouteId(int routeId) {this.routeId = routeId;}

    public double getAccuracy() {return accuracy;}
    public void setAccuracy(double accuracy) {this.accuracy = accuracy;}

    public static Footprint createFootprint(int routeId, Location location) {
        Footprint footprint = new Footprint();
        footprint.setLatitude(location.getLatitude());
        footprint.setLongitude(location.getLongitude());
        footprint.setRouteId(routeId);
        footprint.setFootprintCreationTime(new Date());
        footprint.setAccuracy(location.getAccuracy());
        return footprint;
    }
}


