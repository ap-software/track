package com.track.android.services;

import android.os.SystemClock;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.track.android.entities.Footprint;
import com.track.android.entities.Route;
import com.track.android.services.rest.FootprintClient;
import com.track.android.services.rest.RouteClient;
import com.track.android.utils.Constants;
import com.track.android.utils.ListenerAsyncTask;
import com.track.android.utils.Status;
import com.track.android.utils.TextFeedback;

import java.io.IOException;
import java.net.ConnectException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FootprintService {

	private static final String TAG = "FootprintService";
	private Status completionStatus;

	private static AuthenticationService authenticationService;

	private FootprintService(){}
	
	private static FootprintService instance;
	
	public static FootprintService getInstance(){
		if (instance == null){
			instance = new FootprintService();
		}
		if(authenticationService==null){
			authenticationService = AuthenticationService.getInstance();
		}
		return instance;
	}

	public void addFootprint(OnTaskCompleted listener, final Footprint footprint){
		ListenerAsyncTask doCreateFootprint = new ListenerAsyncTask(listener){

			Boolean isRequestCompleted = false;

			@Override
			protected Object doInBackground(Object... params) {

				try{
                    FootprintClient client = FootprintClient.retrofit.create(FootprintClient.class);
                    Call<TextFeedback> call = client.add(authenticationService.getJsonWebToken().toJSON(),footprint);
                    call.enqueue(new Callback<TextFeedback>() {

						@Override
                        public void onResponse(Call<TextFeedback> call, Response<TextFeedback> response) {
							if (response.isSuccessful()){
								completionStatus = com.track.android.utils.Status.FOOTPRINT_ADDED_SUCCESSFULLY;
								TextFeedback responseError = response.body();
							}else{
								try {
									Gson gson = new GsonBuilder().create();
									TextFeedback responseError = gson.fromJson(response.errorBody().string(),TextFeedback.class);
									completionStatus = com.track.android.utils.Status.ERROR;
									completionStatus.setMessage(responseError.getText());
								} catch (IOException e) {
									completionStatus = com.track.android.utils.Status.ERROR;
									e.printStackTrace();
								}
							}

							isRequestCompleted=true;
						}

                        @Override
                        public void onFailure(Call<TextFeedback> call, Throwable t) {
							try{
								throw t;
							}catch (ConnectException e){
								completionStatus = com.track.android.utils.Status.CONNECTION_FAILED;
							}catch (Throwable e){
								completionStatus = com.track.android.utils.Status.ERROR;
							}

							isRequestCompleted=true;
						}
                    });
				}catch (Exception e){
					isRequestCompleted=true;
					completionStatus = com.track.android.utils.Status.ERROR;
				}

				while (isRequestCompleted==false){
					SystemClock.sleep(Constants.AWAITING_INTERVAL);
				}
                return completionStatus;
			}
		 };
		 doCreateFootprint.execute();
	}

}
