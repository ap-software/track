package com.track.android.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import com.track.android.R;

import android.os.Bundle;
import android.view.View;

public class BeginTracking extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_begin_tracking);
    }

    protected void doShowCreatingRouteActivity(View view) {
        Intent intent = new Intent(this, CreatingRoute.class);
        startActivity(intent);
    }

    protected void doShowRouteHistoryActivity(View view) {
        Intent intent = new Intent(this, RouteHistory.class);
        startActivity(intent);
    }

}
