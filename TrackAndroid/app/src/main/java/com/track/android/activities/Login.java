package com.track.android.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.track.android.R;
import com.track.android.services.AuthenticationService;
import com.track.android.services.OnTaskCompleted;
import com.track.android.utils.Status;

public class Login extends AppCompatActivity implements OnTaskCompleted {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void doAuthenticate(View view){
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarLogin);
        progressBar.setVisibility(View.VISIBLE);
        AuthenticationService authenticationService = AuthenticationService.getInstance();
        EditText usernameEditText = (EditText) findViewById(R.id.username_input);
        EditText passwordEditText= (EditText) findViewById(R.id.password_input);
        TextView errorMessageEditText= (TextView) findViewById(R.id.errorMessage);

        String username;
        username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        if (username==null || username.equals("")){
            progressBar.setVisibility(View.INVISIBLE);
            errorMessageEditText.setText("Username cannot be empty");
            return;
        }
        if (password==null || password.equals("")){
            progressBar.setVisibility(View.INVISIBLE);
            errorMessageEditText.setText("Password cannot be empty");
            return;
        }
        authenticationService.authenticate(this,username, password);
    }

    private void openBeginTrackingActivity(){
        Intent intent = new Intent(this, BeginTracking.class);
        startActivity(intent);
    }

    @Override
    public void onTaskCompleted(final Status completionStatus) {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarLogin);
        progressBar.setVisibility(View.INVISIBLE);

        TextView errorMessageEditText= (TextView) findViewById(R.id.errorMessage);
        errorMessageEditText.setText(completionStatus.getMessage());

        switch (completionStatus){
            case CONNECTION_FAILED:
                //openBeginTrackingActivity();
                break;
            case AUTHENTICATION_FAILED:
                //openBeginTrackingActivity();
                break;
            case AUTHENTICATION_SUCCESS:
                openBeginTrackingActivity();
                break;
            default:
                //openBeginTrackingActivity();
                break;
        }

    }
}
