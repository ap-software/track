package com.track.android.services;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by APOKLADE on 22/01/2017.
 */

public class LocationService {

    private LocationService() {
    }

    private static final String TAG = LocationService.class.getName();

    private static int MIN_TIME_INTERVAL_BETWEEN_LOCATION_UPDATES = 5000;
    private static int MIN_DISTANCE_BETWEEN_LOCATION_UPDATES = 1;

    private static LocationService instance;

    private AppCompatActivity activity;

    // Acquire a reference to the system Location Manager
    private LocationManager locationManagerGps;
    private LocationManager locationManagerNetwork;
    // Define a listener that responds to location updates
    private LocationListener locationListenerGps;
    private LocationListener locationListenerNetwork;

    private Location locationGps;
    private Location locationNetwork;

    public static LocationService getInstance(AppCompatActivity activity) {
        if (instance == null) {
            instance = new LocationService();
        }
        instance.setActivity(activity);
        return instance;
    }

    public void start() {
        initLocationManagerGps();
        initLocationManagerNetwork();
    }

    private void initLocationManagerNetwork() {
        locationManagerNetwork = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        locationListenerNetwork = new LocationListener() {
            public void onLocationChanged(Location location) {
                LocationService.this.locationNetwork = location;
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.i(TAG, "NETWORKonStatusChanged");
            }

            public void onProviderEnabled(String provider) {
                Log.i(TAG, "NETWORKonProviderEnabled");
            }

            public void onProviderDisabled(String provider) {
                Log.i(TAG, "NETWORKonProviderDisabled");
            }
        };
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManagerNetwork.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                MIN_TIME_INTERVAL_BETWEEN_LOCATION_UPDATES,
                MIN_DISTANCE_BETWEEN_LOCATION_UPDATES,
                locationListenerNetwork);
    }

    private void initLocationManagerGps() {
        locationManagerGps = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        locationListenerGps = new LocationListener() {
            public void onLocationChanged(Location location) {
                LocationService.this.locationGps = location;
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.i(TAG, "GPSonStatusChanged");
            }

            public void onProviderEnabled(String provider) {
                Log.i(TAG, "GPSonProviderEnabled");
            }

            public void onProviderDisabled(String provider) {
                Log.i(TAG, "GPSonProviderDisabled");
            }
        };
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManagerGps.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                MIN_TIME_INTERVAL_BETWEEN_LOCATION_UPDATES,
                MIN_DISTANCE_BETWEEN_LOCATION_UPDATES,
                locationListenerGps);
    }

    public void stop() {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if(locationManagerGps!=null){
            locationManagerGps.removeUpdates(locationListenerGps);
            locationManagerGps=null;
            locationListenerGps=null;
        }
        if(locationManagerNetwork!=null){
            locationManagerNetwork.removeUpdates(locationListenerNetwork);
            locationManagerNetwork=null;
            locationListenerNetwork=null;
        }
    }

    public Location getLocationGps(){
        return locationGps;
    }

    public Location getLocationNetwork(){
        return locationNetwork;
    }

    private AppCompatActivity getActivity() {
        return activity;
    }

    private void setActivity(AppCompatActivity activity) {
        this.activity = activity;
    }
}
