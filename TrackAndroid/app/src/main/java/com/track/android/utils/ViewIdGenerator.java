package com.track.android.utils;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by APOKLADE on 10/02/2017.
 */

public class ViewIdGenerator {

    private static int id;

    private ViewIdGenerator(){};

    public static int generate(AppCompatActivity activity){
        View v = activity.findViewById(id);
        while (v != null){
            v = activity.findViewById(++id);
        }
        return id++;
    }

}
